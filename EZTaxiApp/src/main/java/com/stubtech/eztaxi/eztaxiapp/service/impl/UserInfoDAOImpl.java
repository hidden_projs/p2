package com.stubtech.eztaxi.eztaxiapp.service.impl;

import com.stubtech.eztaxi.commonmodels.models.UserModel;
import com.stubtech.eztaxi.eztaxiapp.service.IUserInfoDAO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;

/**
 * Created by Batousik on 14.02.2016.
 */
@Repository
public class UserInfoDAOImpl implements IUserInfoDAO {

    private final static Logger LOGGER = LoggerFactory.getLogger(UserInfoDAOImpl.class);

    @Autowired
    private DataSource dataSource;

    public UserModel getUserInfoById(String username) {
        LOGGER.debug("Get user is called");

        String sql = "SELECT u.username name, u.password pass, ur.rolename role FROM " +
                "users u INNER JOIN user_role ur on u.id=ur.userid WHERE " +
                "u.enabled = 1 and u.username = ?";

        UserModel userInfo = null;
        try {
            userInfo = new JdbcTemplate(this.dataSource).queryForObject(sql, new Object[]{username},
                    (rs, rowNum) -> {
                        UserModel user = new UserModel();
                        user.setUsername(rs.getString("name"));
                        user.setPassword(rs.getString("pass"));
                        user.setRole(rs.getString("role"));
                        return user;
                    });
        } catch (Exception e) {
            LOGGER.error("Failed to fetch user: " + e.getMessage());
        }
        return userInfo;
    }
}
