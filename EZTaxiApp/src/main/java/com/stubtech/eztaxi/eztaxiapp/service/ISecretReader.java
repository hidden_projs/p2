package com.stubtech.eztaxi.eztaxiapp.service;

import javax.crypto.SecretKey;

/**
 * Created by Batousik on 15.02.2016.
 */
public interface ISecretReader {
    SecretKey loadKey();
}
