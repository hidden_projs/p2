package com.stubtech.eztaxi.eztaxiapp.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

import javax.sql.DataSource;


@Configuration
public class DBConfig {
    private static final Logger LOGGER = LoggerFactory.getLogger(DBConfig.class);

    @Autowired
    private Environment environment;

    @Bean
    public DataSource dataSource() {
        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName(environment.getProperty("jdbc.driverClass"));
        dataSource.setUrl(environment.getProperty("jdbc.url"));
        dataSource.setUsername(environment.getProperty("jdbc.username"));
        dataSource.setPassword(environment.getProperty("jdbc.password"));
        return dataSource;
    }

//    @Bean(name = "jndiDataSource")
//    public DataSource dataSourceJndi() {
//        DataSource dataSource = null;
//        try {
//            JndiDataSourceLookup jndiDataSourceLookup = new JndiDataSourceLookup();
//            jndiDataSourceLookup.setResourceRef(true);
//            dataSource = jndiDataSourceLookup.getDataSource("jdbc/EZTaxiDB");
//            LOGGER.debug("Created DS");
//        } catch (Exception e) {
//            LOGGER.error("No DS: " + e.getMessage());
//        }
//        return dataSource;
//    }
}
