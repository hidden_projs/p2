package com.stubtech.eztaxi.eztaxiapp.service;

import com.stubtech.eztaxi.commonmodels.models.UserModel;

/**
 * Created by Batousik on 14.02.2016.
 */
public interface IUserInfoDAO {
    UserModel getUserInfoById(String username);
}
