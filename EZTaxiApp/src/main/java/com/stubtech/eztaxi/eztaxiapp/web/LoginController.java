package com.stubtech.eztaxi.eztaxiapp.web;

import com.stubtech.eztaxi.eztaxiapp.config.Security.TokenHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletResponse;

@Controller
@RequestMapping(value = "/auth/login")
public class LoginController {

    private static final Logger LOGGER = LoggerFactory.getLogger(LoginController.class);

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private TokenHandler tokenHandler;

    @RequestMapping(method = RequestMethod.POST)
    public void attemptLogin(@RequestParam("un") String un, @RequestParam("pw") String pw, HttpServletResponse response) {
        // @RequestHeader(value="User-Agent") String userAgent
//
        Authentication result = null;
        try {
            Authentication authrequest = new UsernamePasswordAuthenticationToken(un, pw);
            result = authenticationManager.authenticate(authrequest);
            SecurityContext sc = SecurityContextHolder.getContext();
            sc.setAuthentication(result);
            replySuccess(response, result);

            LOGGER.info("Successfully authenticated. Security context contains: " +
                    SecurityContextHolder.getContext().getAuthentication());

        } catch (AuthenticationException e) {
            LOGGER.error("Authentication failed: " + e.getMessage());
            replyFail(response, result);
        }
    }

    private void replySuccess(HttpServletResponse response, Authentication result) {
        response.setStatus(200);
        String token = tokenHandler.createTokenForUser((User) result.getPrincipal());
        response.addHeader("X-AUTH-TOKEN", token);
        response.addHeader("AUTHSTATUS", String.valueOf(result.isAuthenticated()));
    }

    private void replyFail(HttpServletResponse response, Authentication result) {
        response.setStatus(403);
        response.addHeader("X-AUTH-TOKEN", "");
        response.addHeader("AUTHSTATUS", String.valueOf(result.isAuthenticated()));
    }
}


