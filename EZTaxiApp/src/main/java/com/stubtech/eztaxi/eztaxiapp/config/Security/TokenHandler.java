package com.stubtech.eztaxi.eztaxiapp.config.Security;

import com.stubtech.eztaxi.eztaxiapp.service.impl.SecretReaderImpl;
import com.stubtech.eztaxi.eztaxiapp.service.impl.UserDetailsServiceImpl;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Component;

@Component
public final class TokenHandler {

    @Autowired
    private SecretReaderImpl secretReader;

    @Autowired
    private UserDetailsServiceImpl userDetailsService;

    public User parseUserFromToken(String token) {
        String username = Jwts.parser()
                .setSigningKey(secretReader.loadKey())
                .parseClaimsJws(token)
                .getBody()
                .getSubject();
        return (User) userDetailsService.loadUserByUsername(username);
    }

    public String createTokenForUser(User user) {
        return Jwts.builder()
                .setSubject(user.getUsername())
                .signWith(SignatureAlgorithm.HS512, secretReader.loadKey())
                .compact();
    }
}
