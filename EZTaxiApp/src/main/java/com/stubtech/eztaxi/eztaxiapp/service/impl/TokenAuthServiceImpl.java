package com.stubtech.eztaxi.eztaxiapp.service.impl;

import com.stubtech.eztaxi.eztaxiapp.config.Security.TokenHandler;
import com.stubtech.eztaxi.eztaxiapp.config.Security.UserAuthentication;
import com.stubtech.eztaxi.eztaxiapp.service.ITokenAuthService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;

@Service
public class TokenAuthServiceImpl implements ITokenAuthService {

    private static final String AUTH_HEADER_NAME = "X-AUTH-TOKEN";
    private static final Logger LOGGER = LoggerFactory.getLogger(UserInfoDAOImpl.class);

    @Autowired
    private TokenHandler tokenHandler;

    public Authentication getAuthentication(HttpServletRequest request) {
        LOGGER.debug("TokenAuthService is used");
        final String token = request.getHeader(TokenAuthServiceImpl.AUTH_HEADER_NAME);
        if (token != null) {
            final User user = tokenHandler.parseUserFromToken(token);
            if (user != null) {
                return new UserAuthentication(user);
            }
        }
        return null;
    }
}
