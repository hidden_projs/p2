package com.stubtech.eztaxi.eztaxiapp.web.client;

import com.stubtech.eztaxi.eztaxiapp.domain.TaxiOrderForm;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping(value = "/client/ordertaxi")
public class OrderTaxiController {
    private static final Logger LOGGER = LoggerFactory.getLogger(OrderTaxiController.class);

    @RequestMapping(method = RequestMethod.POST)
    public void makeTaxiOrder(@RequestParam("json") TaxiOrderForm taxiOrderForm) {
        LOGGER.info("Order was tried: " + taxiOrderForm.toString());
    }
}
