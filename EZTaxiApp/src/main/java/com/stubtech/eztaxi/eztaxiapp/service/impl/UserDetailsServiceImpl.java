package com.stubtech.eztaxi.eztaxiapp.service.impl;

import com.stubtech.eztaxi.commonmodels.models.UserModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Arrays;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {

    @Autowired
    private UserInfoDAOImpl userInfoDAOImpl;

    public UserDetails loadUserByUsername(String username)
            throws UsernameNotFoundException {
        UserModel userInfo = userInfoDAOImpl.getUserInfoById(username);
        GrantedAuthority authority = new SimpleGrantedAuthority(userInfo.getRole());
        final User user = new User(userInfo.getUsername(),
                userInfo.getPassword(), Arrays.asList(authority));
        //TODO: AccountStatusUserDetailsChecker
        return user;
    }
}
