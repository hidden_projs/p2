package com.stubtech.eztaxi.eztaxiapp.service;

import org.springframework.security.core.Authentication;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by Batousik on 14.02.2016.
 */
public interface ITokenAuthService {
    Authentication getAuthentication(HttpServletRequest request);
}
