package com.stubtech.eztaxi.eztaxiapp.service.impl;

import com.stubtech.eztaxi.eztaxiapp.service.ISecretReader;
import org.springframework.stereotype.Service;

import javax.crypto.SecretKey;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableEntryException;
import java.security.cert.CertificateException;

/**
 * Created by Batousik on 15.02.2016.
 */
@Service
public class SecretReaderImpl implements ISecretReader {

    public SecretKey loadKey() {
        ClassLoader classLoader = getClass().getClassLoader();
        File file = new File(classLoader.getResource("keys/keys.jceks").getFile());
        char[] password = "alena1994".toCharArray();
        String alias = "desedekey";
        FileInputStream fIn;
        KeyStore keystore;
        KeyStore.Entry entry = null;
        try {
            fIn = new FileInputStream(file);
            keystore = KeyStore.getInstance("JCEKS");
            keystore.load(fIn, password);
            entry = keystore.getEntry(alias, new KeyStore.PasswordProtection(password));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (CertificateException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (UnrecoverableEntryException e) {
            e.printStackTrace();
        } catch (KeyStoreException e) {
            e.printStackTrace();
        }
        return ((KeyStore.SecretKeyEntry) entry).getSecretKey();
    }
}
