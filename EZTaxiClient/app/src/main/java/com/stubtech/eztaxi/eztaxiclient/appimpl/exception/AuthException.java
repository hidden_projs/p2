package com.stubtech.eztaxi.eztaxiclient.appimpl.exception;

/**
 * Custom and useless exception, maybe a bit useful for debugging,
 * whenever, her Highness, LogCat decides to work.
 */
public class AuthException extends Exception {

    public AuthException(String detailMessage) {
        super(detailMessage);
    }

    public AuthException(Throwable throwable) {
        super(throwable);
    }
}
