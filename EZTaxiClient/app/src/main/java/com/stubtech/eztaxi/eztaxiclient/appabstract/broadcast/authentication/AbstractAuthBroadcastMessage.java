package com.stubtech.eztaxi.eztaxiclient.appabstract.broadcast.authentication;

import android.os.Bundle;
import android.support.annotation.NonNull;

import com.google.common.base.Preconditions;
import com.stubtech.eztaxi.eztaxiclient.appinterface.broadcast.auth.IAuthBroadcastMessage;

/**
 * Extracted Abstract class for communication with android account manager
 */
public abstract class AbstractAuthBroadcastMessage implements IAuthBroadcastMessage {

    private final Bundle data;

    public AbstractAuthBroadcastMessage(Bundle data) {
        Preconditions.checkNotNull(data, "Data is null");
        this.data = data;
    }

    @Override
    @NonNull
    public Bundle getData() {
        return data;
    }
}
