package com.stubtech.eztaxi.eztaxiclient.appimpl.architecture.view;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.stubtech.eztaxi.eztaxiclient.R;
import com.stubtech.eztaxi.eztaxiclient.appanotation.ui.ActivityScope;
import com.stubtech.eztaxi.eztaxiclient.appimpl.architecture.controller.LoginControllerImpl;
import com.stubtech.eztaxi.eztaxiclient.appimpl.dependencyInjection.DependencyInjector;
import com.stubtech.eztaxi.eztaxiclient.appinterface.architecture.view.ILoginView;
import com.stubtech.eztaxi.eztaxiclient.apputil.Constants;
import com.stubtech.eztaxi.eztaxiclient.apputil.Utils;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * This class provides login ui implementation for the application
 * in bundle With Android Account Manager, When sign in to eztaxi account is required,
 * it will route to this activity just like facebook or google
 */
public class LoginActivity extends AppCompatActivity implements ILoginView {

    @Inject
    @ActivityScope
    LoginControllerImpl loginActivityController;

    @Bind(R.id.login_btn_login)
    AppCompatButton mBtnLogin;

    @Bind(R.id.login_link_register)
    TextView mRegisterLink;

    @Bind(R.id.login_til_un)
    TextInputLayout mTilUN;
    @Bind(R.id.app_start_til_pw)
    TextInputLayout mTilPW;

    @Bind(R.id.login_txt_un)
    TextView mTxtUN;
    @Bind(R.id.app_start_txt_pw)
    TextView mTxtPW;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_app_start);
        injectMembers();
        bindViews();
        mTilUN.setErrorEnabled(true);
        mTilPW.setErrorEnabled(true);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        loginActivityController.destroy();
    }

    @OnClick(R.id.app_start_btn_login)
    void onBtnLoginClick(View view) {
        Log.d(Constants.DEBUG_TAG, "Login");
        loginActivityController.requestAccess();
    }

    @OnClick(R.id.app_start_link_register)
    void onLinkRegisterClick(View view) {
        Intent intent = new Intent(getApplicationContext(), RegisterActivity.class);
        startActivity(intent);
        onDestroy();
    }

    @NonNull
    @Override
    public String getUN() {
        return mTxtUN.getText().toString().trim();
    }

    @NonNull
    @Override
    public String getPW() {
        return mTxtPW.getText().toString().trim();
    }

    @NonNull
    @Override
    public String getAuthTokenType() {
        return authTokenType;
    }

    @Override
    public void onErrorOccured(@NonNull String message) {
        Utils.showToast(this, message);
    }

    public void bindViews() {
        ButterKnife.bind(this);
    }

    public void injectMembers() {
        DependencyInjector.activityComponent(this).inject(this);
    }

    @Override
    public boolean doSecurityLogic() {
        Log.d(Constants.DEBUG_TAG, logMessage);
        this.startActivity(intent);
        this.finishActivity(0);
        return false;
    }

    public AppCompatButton getmBtnLogin() {
        return mBtnLogin;
    }

    public TextView getmRegisterLink() {
        return mRegisterLink;
    }

    public TextInputLayout getmTilUN() {
        return mTilUN;
    }

    public TextInputLayout getmTilPW() {
        return mTilPW;
    }

    public TextView getmTxtUN() {
        return mTxtUN;
    }

    public TextView getmTxtPW() {
        return mTxtPW;
    }
}
