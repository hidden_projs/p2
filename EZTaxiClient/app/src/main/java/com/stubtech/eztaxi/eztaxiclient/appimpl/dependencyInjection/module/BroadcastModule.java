package com.stubtech.eztaxi.eztaxiclient.appimpl.dependencyInjection.module;

import com.stubtech.eztaxi.eztaxiclient.BuildConfig;
import com.stubtech.eztaxi.eztaxiclient.appimpl.broadcast.BroadcastManagerImpl;
import com.stubtech.eztaxi.eztaxiclient.appinterface.broadcast.IBroadcastManager;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import de.greenrobot.event.EventBus;

/**
 * This module provides:
 * <p/>
 * Enhancement over Intents
 * Android optimized event bus that simplifies communication between Activities, Fragments,
 * Threads, Services, etc. Less code, better quality. http://greenrobot.org/eventbus/
 * <p/>
 * API describes it enough :)
 */
@Module
public class BroadcastModule {

    @Provides
    @Singleton
    public IBroadcastManager provideBroadcastManager() {
        final boolean isDebugBuild = BuildConfig.DEBUG;
        final EventBus eventBus = EventBus.builder()
                .logNoSubscriberMessages(isDebugBuild)
                .logSubscriberExceptions(isDebugBuild)
                .build();
        return new BroadcastManagerImpl(eventBus);
    }
}
