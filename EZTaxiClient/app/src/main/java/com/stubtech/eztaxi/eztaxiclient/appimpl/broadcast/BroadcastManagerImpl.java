package com.stubtech.eztaxi.eztaxiclient.appimpl.broadcast;

import android.support.annotation.NonNull;

import com.google.common.base.Preconditions;
import com.stubtech.eztaxi.eztaxiclient.appinterface.broadcast.IBroadcastManager;

import de.greenrobot.event.EventBus;

/**
 * Implementation class for broadcasting objects
 *
 * @see com.stubtech.eztaxi.eztaxiclient.appinterface.broadcast.IBroadcastManager
 */
public class BroadcastManagerImpl implements IBroadcastManager {

    private final EventBus eventBus;

    public BroadcastManagerImpl(EventBus eventBus) {
        Preconditions.checkNotNull(eventBus);
        this.eventBus = eventBus;
    }

    @Override
    public void register(@NonNull Object object) {
        Preconditions.checkNotNull(object);
        if (!eventBus.isRegistered(object)) {
            eventBus.register(object);
        }
    }

    @Override
    public void unregister(@NonNull Object object) {
        Preconditions.checkNotNull(object);
        eventBus.unregister(object);
    }

    @Override
    public void post(@NonNull Object object) {
        Preconditions.checkNotNull(object);
        eventBus.post(object);
    }

    @Override
    public void destroy() {
    }
}
