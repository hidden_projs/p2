package com.stubtech.eztaxi.eztaxiclient.appinterface.dependencyinjection;

import com.stubtech.eztaxi.eztaxiclient.appanotation.ui.ActivityScope;
import com.stubtech.eztaxi.eztaxiclient.appimpl.architecture.view.HomeActivity;
import com.stubtech.eztaxi.eztaxiclient.appimpl.architecture.view.LoginActivity;
import com.stubtech.eztaxi.eztaxiclient.appimpl.architecture.view.RegisterActivity;
import com.stubtech.eztaxi.eztaxiclient.appimpl.architecture.view.chooseaccount
        .ChooseAccountActivity;
import com.stubtech.eztaxi.eztaxiclient.appimpl.dependencyInjection.module.ActivityModule;

import dagger.Component;

/**
 * Defines Dagger graph for the activity scope
 */
@ActivityScope
@Component(dependencies = IDependencyGraph.class,
        modules = {ActivityModule.class})
public interface IActivityComponent {

    void inject(HomeActivity homeActivity);
    void inject(RegisterActivity registerActivity);
    void inject(ChooseAccountActivity chooseAccountActivity);
    void inject(LoginActivity loginActivity);

}
