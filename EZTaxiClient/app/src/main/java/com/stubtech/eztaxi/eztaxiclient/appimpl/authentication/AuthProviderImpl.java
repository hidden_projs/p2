package com.stubtech.eztaxi.eztaxiclient.appimpl.authentication;

import android.support.annotation.NonNull;

import com.google.common.base.Preconditions;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.RequestBody;
import com.stubtech.eztaxi.eztaxiclient.appabstract.authentication.UserCredentials;
import com.stubtech.eztaxi.eztaxiclient.appimpl.architecture.model.ResponseParser;
import com.stubtech.eztaxi.eztaxiclient.appimpl.exception.AuthException;
import com.stubtech.eztaxi.eztaxiclient.appinterface.authentication.IAuthParser;
import com.stubtech.eztaxi.eztaxiclient.appinterface.authentication.IAuthProvider;
import com.stubtech.eztaxi.eztaxiclient.apputil.Constants;
import com.stubtech.eztaxi.eztaxiclient.apputil.HttpStatusCodes;

import org.json.JSONException;
import org.json.JSONObject;

import javax.inject.Inject;
import javax.inject.Named;

import retrofit.Call;
import retrofit.MoshiConverterFactory;
import retrofit.Response;
import retrofit.Retrofit;

/**
 * Auth Provider with remote service
 */
public class AuthProviderImpl implements IAuthProvider {

    private static final String BASE_URL = "192.168.0.13:8080/eztaxiserver/";
    private static final String MEDIA_TYPE_APPLICATION_JSON = "application/json";
    static {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(Constants.Parse.PARAMETER_USERNAME, "%s");
            jsonObject.put(Constants.Parse.PARAMETER_PASSWORD, "%s");
        } catch (JSONException e) {
            e.printStackTrace();
            throw new IllegalStateException("Could not create Http Post request!");
        }
    }
    @Inject
    @Named("AuthenticatorProvider")
    IAuthParser authParserImpl;

    public AuthProviderImpl(final @NonNull OkHttpClient okHttpClient) {
        authParserImpl = new Retrofit.Builder().baseUrl(BASE_URL)
                .addConverterFactory(MoshiConverterFactory.create())
                .client(okHttpClient)
                .build()
                .create(IAuthParser.class);
    }

    @Override
    public String register(UserCredentials userCredentials) throws AuthException {
        final String un = userCredentials.getUN();
        final String pw = userCredentials.getPW();
        final String email = userCredentials.getEmail();
        final RequestBody requestBody = createRegisterRequestBody(un, pw, email);
        final Call<ResponseParser> call = authParserImpl.register(requestBody);
        try {
            final Response<ResponseParser> response = call.execute();
            Preconditions.checkState(response.code() == HttpStatusCodes.SC_CREATED);
            final ResponseParser responseParser = response.body();
            return responseParser.sessionToken;
        } catch (Exception e) {
            throw new AuthException(e);
        }
    }

    private RequestBody createRegisterRequestBody(String un, String pw, String email) {
        final JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(Constants.Parse.PARAMETER_USERNAME, un);
            jsonObject.put(Constants.Parse.PARAMETER_PASSWORD, pw);
            jsonObject.put(Constants.Parse.PARAMETER_EMAIL, email);
            final MediaType mediaType = MediaType.parse(MEDIA_TYPE_APPLICATION_JSON);
            final String content = jsonObject.toString();
            return RequestBody.create(mediaType, content);
        } catch (JSONException e) {
            e.printStackTrace();
            throw new IllegalStateException("Could not create Http Post request!");
        }
    }

    @Override
    public String login(@NonNull UserCredentials userCredentials) throws AuthException {
        final String un = userCredentials.getUN();
        final String pw = userCredentials.getPW();
        final RequestBody requestBody = createLoginRequestBody(un, pw);
        final Call<ResponseParser> call = authParserImpl.register(requestBody);
        try {
            final Response<ResponseParser> response = call.execute();
            Preconditions.checkState(response.code() == HttpStatusCodes.SC_CREATED);
            final ResponseParser responseParser = response.body();
            return responseParser.sessionToken;
        } catch (Exception e) {
            throw new AuthException(e);
        }
    }

    private RequestBody createLoginRequestBody(String un, String pw) {
        final JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(Constants.Parse.PARAMETER_USERNAME, un);
            jsonObject.put(Constants.Parse.PARAMETER_PASSWORD, pw);
            final MediaType mediaType = MediaType.parse(MEDIA_TYPE_APPLICATION_JSON);
            final String content = jsonObject.toString();
            return RequestBody.create(mediaType, content);
        } catch (JSONException e) {
            e.printStackTrace();
            throw new IllegalStateException("Could not create Http Post request!");
        }
    }

    @Override
    public void destroy() {
    }
}
