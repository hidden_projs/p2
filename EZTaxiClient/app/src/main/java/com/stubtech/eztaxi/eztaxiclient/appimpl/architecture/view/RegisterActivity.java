package com.stubtech.eztaxi.eztaxiclient.appimpl.architecture.view;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.util.Log;
import android.widget.EditText;

import com.stubtech.eztaxi.commonmodel.model.UserModel;
import com.stubtech.eztaxi.eztaxiclient.R;
import com.stubtech.eztaxi.eztaxiclient.appanotation.ui.ActivityScope;
import com.stubtech.eztaxi.eztaxiclient.appimpl.architecture.controller.RegisterControllerImpl;
import com.stubtech.eztaxi.eztaxiclient.appimpl.dependencyInjection.DependencyInjector;
import com.stubtech.eztaxi.eztaxiclient.appinterface.architecture.view.IRegisterView;
import com.stubtech.eztaxi.eztaxiclient.apputil.Constants;
import com.stubtech.eztaxi.eztaxiclient.old.ConnectionChecker;
import com.stubtech.eztaxi.eztaxiclient.old.UserRegisterTask;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 *
 */
public class RegisterActivity extends AppCompatActivity implements IRegisterView {

    @Inject
    @ActivityScope
    RegisterControllerImpl registerController;

    @Bind(R.id.register_btn_register)
    AppCompatButton mBtnRegister;
    @Bind(R.id.register_txt_un)
    EditText mTxtUN;
    @Bind(R.id.register_txt_pw)
    EditText mTxtPW;
    @Bind(R.id.register_til_un)
    TextInputLayout mWrapperUN;
    @Bind(R.id.register_til_pw)
    TextInputLayout mWrapperPW;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        injectMembers();
        bindViews();
        prepare();
    }

    private void prepare() {

    }

    private void attemptRegister() {
        UserRegisterTask userRegisterTask;
        UserModel userModel = new UserModel();
        String un;
        String pw;
        un = mTxtUN.getText().toString();
        pw = mTxtPW.getText().toString();
        userModel.setUsername(un);
        userModel.setPassword(pw);
        if (ConnectionChecker.hasConnection(getApplicationContext())) {
            userRegisterTask = new UserRegisterTask(this);
            userRegisterTask.execute(userModel);
        } else {
            Log.i(Constants.DEBUG_TAG, "No internet connection");
            // show no connection dialog
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @NonNull
    @Override
    public String getUsername() {
        return null;
    }

    @NonNull
    @Override
    public String getPassword() {
        return null;
    }

    @NonNull
    @Override
    public String getEmail() {
        return null;
    }

    @Override
    public void showToast(@Nullable String message) {
    }

    @Override
    public void destroy() {
        super.onDestroy();
        registerController.destroy();
    }

    @Override
    public void bindViews() {
        ButterKnife.bind(this);
    }

    @Override
    public void injectMembers() {
        DependencyInjector.activityComponent(this).inject(this);
    }
}
