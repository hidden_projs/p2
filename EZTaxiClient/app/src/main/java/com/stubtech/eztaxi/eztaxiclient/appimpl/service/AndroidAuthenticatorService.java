package com.stubtech.eztaxi.eztaxiclient.appimpl.service;

import android.accounts.AbstractAccountAuthenticator;
import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;

import com.stubtech.eztaxi.eztaxiclient.appimpl.authentication.EZTaxiClientAuthImpl;
import com.stubtech.eztaxi.eztaxiclient.apputil.Constants;

/**
 * Created by Batousik on 28.02.2016.
 */
public class AndroidAuthenticatorService extends Service {

    private AbstractAccountAuthenticator authenticator;

    @Override
    public void onCreate() {
        super.onCreate();
        android.os.Debug.waitForDebugger();
        Log.e(Constants.DEBUG_TAG, "Created Intent");
        authenticator = new EZTaxiClientAuthImpl(this);
    }

    @Override
    public IBinder onBind(Intent intent) {
        return authenticator.getIBinder();
    }
}
