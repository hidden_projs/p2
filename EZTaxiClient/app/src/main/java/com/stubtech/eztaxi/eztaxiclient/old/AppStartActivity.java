package com.stubtech.eztaxi.eztaxiclient.old;

import android.support.v7.app.AppCompatActivity;


public class AppStartActivity extends AppCompatActivity {

    @Override
    protected void onStart() {
        super.onStart();
        startApp();
    }

    private void startApp() {
        MyAuthentificator.reRouteUnAuthenticated(this);
    }
}
