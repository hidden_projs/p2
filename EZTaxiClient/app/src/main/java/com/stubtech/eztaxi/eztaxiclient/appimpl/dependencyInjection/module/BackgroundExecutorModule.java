package com.stubtech.eztaxi.eztaxiclient.appimpl.dependencyInjection.module;

import com.stubtech.eztaxi.eztaxiclient.appimpl.controller.BackgroundExecutorImpl;
import com.stubtech.eztaxi.eztaxiclient.appinterface.controller.IBackgroundExecutor;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * This module provides:
 * <p/>
 * Enhancement for android AsyncTask.
 * <p/>
 * ReactiveX is a combination of the best ideas from the Observer pattern, the Iterator pattern,
 * and functional programming
 * <p/>
 * Easily create event streams or data streams.
 * Compose and transform streams with query-like operators.
 * Subscribe to any observable stream to perform side effects.
 */
@Module
public class BackgroundExecutorModule {

    @Provides
    @Singleton
    IBackgroundExecutor provideBackgroundExecutor() {
        return new BackgroundExecutorImpl();
    }
}
