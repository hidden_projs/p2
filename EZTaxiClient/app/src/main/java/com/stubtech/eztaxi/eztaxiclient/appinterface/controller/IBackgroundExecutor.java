package com.stubtech.eztaxi.eztaxiclient.appinterface.controller;

import com.stubtech.eztaxi.eztaxiclient.apputil.Destroyable;

import rx.functions.Action0;

/**
 * Interface to perform actions on separate thread from main ui thread.
 * Substitutes AsyncTask
 */
public interface IBackgroundExecutor extends Destroyable {

    void execute(Action0 action);
    void destroy();
}
