package com.stubtech.eztaxi.eztaxiclient.appinterface.architecture.controller;

import com.stubtech.eztaxi.eztaxiclient.apputil.Destroyable;

/**
 * Created by Batousik on 28.02.2016.
 */
public interface IRegisterController extends Destroyable {

    void register();
}
