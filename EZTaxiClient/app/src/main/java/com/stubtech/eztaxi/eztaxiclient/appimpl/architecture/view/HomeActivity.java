package com.stubtech.eztaxi.eztaxiclient.appimpl.architecture.view;

import android.accounts.Account;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.stubtech.eztaxi.commonmodel.model.TaxiOrder;
import com.stubtech.eztaxi.eztaxiclient.R;
import com.stubtech.eztaxi.eztaxiclient.appanotation.ui.ActivityScope;
import com.stubtech.eztaxi.eztaxiclient.appimpl.architecture.controller.HomeControllerImpl;
import com.stubtech.eztaxi.eztaxiclient.appimpl.dependencyInjection.DependencyInjector;
import com.stubtech.eztaxi.eztaxiclient.appinterface.architecture.view.IButterKnifeSupportingView;
import com.stubtech.eztaxi.eztaxiclient.appinterface.architecture.view
        .IDaggerInjetionSupportingView;
import com.stubtech.eztaxi.eztaxiclient.appinterface.architecture.view.IHomeView;
import com.stubtech.eztaxi.eztaxiclient.apputil.Constants;
import com.stubtech.eztaxi.eztaxiclient.old.AppAuthentificator;
import com.stubtech.eztaxi.eztaxiclient.old.ConnectionChecker;
import com.stubtech.eztaxi.eztaxiclient.old.TaxiOrderTask;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class HomeActivity extends AppCompatActivity implements IHomeView,
                                                               IDaggerInjetionSupportingView,
                                                               IButterKnifeSupportingView {

    @Bind(R.id.home_btn_logout)
    Button mBtnLogout;
    @Bind(R.id.home_btn_order_taxi)
    Button mBtnOrderTaxi;
    @Inject
    @ActivityScope
    HomeControllerImpl homeControllerImpl;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        android.os.Debug.waitForDebugger();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        bindViews();
        injectMembers();
    }

    public void bindViews() {
        ButterKnife.bind(this);
    }

    public void injectMembers() {
        DependencyInjector.activityComponent(this).inject(this);
    }

    private void prepare() {
        mBtnLogout = (Button) findViewById(R.id.home_btn_logout);
        mBtnOrderTaxi = (Button) findViewById(R.id.home_btn_order_taxi);
        mBtnLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                logout();
            }
        });
        mBtnOrderTaxi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                orderTaxi();
            }
        });
    }

    @Override
    protected void onDestroy() {
        homeControllerImpl.destroy();
        super.onDestroy();
    }

    @OnClick(R.id.home_btn_order_taxi)
    void onOrderTaxiButtonClick(View view) {
        homeControllerImpl.orderTaxi();
    }

    @OnClick(R.id.home_btn_logout)
    void onLogoutButtonClick(View view) {
        homeControllerImpl.logout();
    }

    private void orderTaxi() {
        TaxiOrderTask taxiOrderTask;
        TaxiOrder taxiOrder = new TaxiOrder();
        taxiOrder.setAddressFrom("69 Roundhill Road, KY16 8HE");
        taxiOrder.setAddressTo("10, 9 new park place");
        taxiOrder.setCarType("6 Seater");
        taxiOrder.setComment("No Smoking cab");
        taxiOrder.setPaymentType("Cash");
        taxiOrder.setPickUpTimeMillis(System.currentTimeMillis() + 10000000);
        if (ConnectionChecker.hasConnection(getApplicationContext())) {
            taxiOrderTask = new TaxiOrderTask(this);
            taxiOrderTask.execute(taxiOrder);
        } else {
            Log.i(Constants.DEBUG_TAG, "No internet connection");
            // show no connection dialog
        }
    }

    private void logout() {
        AppAuthentificator.setLoginState(getApplicationContext(), false);
        Intent intent = new Intent(this, LoginActivity2.class);
        this.startActivity(intent);
        this.finishActivity(0);
        return;
    }

    @Override
    public void onAccountAdded() {
    }

    @Override
    public void onErrorOccured(@NonNull String message) {
    }

    @Override
    public void onRequestedTokenObtained(@Nullable String authToken) {
    }

    @Override
    public void onAccountRemoved(@NonNull Account account) {
    }

    @Override
    public void onAuthTokenLoaded(@NonNull String authtoken) {
    }

    @Override
    public void onAuthTokenInvalidated(@NonNull Account account) {
    }

    @Override
    public void onNoAccountsCreated() {
    }
}
