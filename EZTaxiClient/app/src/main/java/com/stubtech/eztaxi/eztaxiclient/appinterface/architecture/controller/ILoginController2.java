package com.stubtech.eztaxi.eztaxiclient.appinterface.architecture.controller;

import android.os.Bundle;
import android.support.annotation.NonNull;

import com.stubtech.eztaxi.eztaxiclient.apputil.Destroyable;

/**
 * Contract for Login Activity Controller
 */
public interface ILoginController2 extends Destroyable {

    void login();
    void notifyLoggedIn(@NonNull Bundle data);
}
