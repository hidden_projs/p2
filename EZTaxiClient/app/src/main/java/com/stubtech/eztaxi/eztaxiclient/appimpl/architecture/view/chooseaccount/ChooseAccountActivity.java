package com.stubtech.eztaxi.eztaxiclient.appimpl.architecture.view.chooseaccount;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextSwitcher;
import android.widget.TextView;
import android.widget.ViewSwitcher;

import com.stubtech.eztaxi.eztaxiclient.R;
import com.stubtech.eztaxi.eztaxiclient.appanotation.ui.ActivityScope;
import com.stubtech.eztaxi.eztaxiclient.appimpl.architecture.controller.ChooseAccountController;
import com.stubtech.eztaxi.eztaxiclient.appimpl.dependencyInjection.DependencyInjector;
import com.stubtech.eztaxi.eztaxiclient.appinterface.architecture.view.IButterKnifeSupportingView;
import com.stubtech.eztaxi.eztaxiclient.appinterface.architecture.view
        .IDaggerInjetionSupportingView;

import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import it.moondroid.coverflow.components.ui.containers.FeatureCoverFlow;

public class ChooseAccountActivity extends ActionBarActivity implements
                                                             IDaggerInjetionSupportingView,
                                                             IButterKnifeSupportingView {

    @Inject
    @ActivityScope
    ChooseAccountController chooseAccountController;

    CoverFlowAdapter mAdapter;
    ArrayList<GameEntity> mContacts = new ArrayList<>(0);

    @Bind(R.id.choose_account_coverflow)
    FeatureCoverFlow mCoverFlow;

    @Bind(R.id.title)
    TextSwitcher mTitle;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.avtivity_chooseaccount);
        bindViews();
        injectMembers();
       // prepareCoverFlow();
    }

    public void prepareCoverFlow(){
        loadContacts();
        mTitle.setFactory(new ViewSwitcher.ViewFactory() {
            @Override
            public View makeView() {
                LayoutInflater inflater = LayoutInflater.from(ChooseAccountActivity.this);
                TextView textView = (TextView) inflater.inflate(R.layout.item_title, null);
                return textView;
            }
        });

        mAdapter = new CoverFlowAdapter(this);
        mAdapter.setData(mContacts);

        mCoverFlow.setAdapter(mAdapter);
//        mCoverFlow.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//                Toast.makeText(ChooseAccountActivity.this, getResources().getString(mData.get
//                        (position).titleResId), Toast.LENGTH_SHORT)
//                        .show();
//            }
//        });
        mCoverFlow.setOnScrollPositionListener(new FeatureCoverFlow.OnScrollPositionListener() {
            @Override
            public void onScrolledToPosition(int position) {
                mTitle.setText("Dfg");//getResources().getString(mData.get(position).titleResId));
            }

            @Override
            public void onScrolling() {
                mTitle.setText("");
            }
        });
    }

    private void loadContacts() {

        mContacts.add(new GameEntity(R.drawable.background6, 1));
        mContacts.add(new GameEntity(R.drawable.background6, 2));
    }

    @Override
    public void bindViews() {
        ButterKnife.bind(this);
    }

    @Override
    public void injectMembers() {
        DependencyInjector.activityComponent(this).inject(this);
    }
}
