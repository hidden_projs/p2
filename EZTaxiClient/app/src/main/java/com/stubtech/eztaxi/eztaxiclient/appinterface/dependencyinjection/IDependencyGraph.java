package com.stubtech.eztaxi.eztaxiclient.appinterface.dependencyinjection;

import android.accounts.AccountManager;

import com.squareup.leakcanary.RefWatcher;
import com.stubtech.eztaxi.eztaxiclient.appimpl.authentication.EZTaxiClientAuthImpl;
import com.stubtech.eztaxi.eztaxiclient.appimpl.dependencyInjection.module.ApplicationModule;
import com.stubtech.eztaxi.eztaxiclient.appimpl.dependencyInjection.module.AuthenticationModule;
import com.stubtech.eztaxi.eztaxiclient.appimpl.dependencyInjection.module.BackgroundExecutorModule;
import com.stubtech.eztaxi.eztaxiclient.appimpl.dependencyInjection.module.BroadcastModule;
import com.stubtech.eztaxi.eztaxiclient.appimpl.dependencyInjection.module.HttpModule;
import com.stubtech.eztaxi.eztaxiclient.appimpl.dependencyInjection.module
        .MemoryLeakDetectionModule;
import com.stubtech.eztaxi.eztaxiclient.appimpl.service.AuthService;
import com.stubtech.eztaxi.eztaxiclient.appinterface.authentication.IAccountManagerHelper;
import com.stubtech.eztaxi.eztaxiclient.appinterface.broadcast.IBroadcastManager;
import com.stubtech.eztaxi.eztaxiclient.appinterface.controller.IBackgroundExecutor;

import javax.inject.Singleton;

import dagger.Component;
import dagger.Lazy;

/**
 * The injector class for Dagger.
 * <p/>
 * Assigns references in activities, services, or fragments to have access to singletons
 * <p/>
 * Singleton -> single value created for all usages aka Application Scoped
 * Component -> marks class for Dagger to be included for dependency injection
 */
@Singleton
/**
 * Explicitly Inject Modules, modules are like Config files in spring,
 * Inside which I can create Beans
 */
@Component(modules = {
        // Main Application module
        ApplicationModule.class,
        // Authentication Module
        AuthenticationModule.class,
        // Module used to exchange messages
        BroadcastModule.class,
        // Module used to communicate over HTTP
        HttpModule.class,
        // Module used to execute tasks in background, not on the main ui thread
        BackgroundExecutorModule.class,
        // Module used for debug purposes
        MemoryLeakDetectionModule.class})
public interface IDependencyGraph {

    /*
     * To inject fields in a class it has to call inject() method.
     * Each such class has to have explicit definition below
     */
    void inject(EZTaxiClientAuthImpl ezTaxiClientAuthImpl);
    void inject(AuthService authService);

    /*
     * When creating dependent components, the parent component needs to explicitly expose the
     * objects to downstream objects. For example, if a downstream component needed access to the
     * Retrofit instance, it would need to explicitly expose it with the corresponding return type
     *
     * Eg my dependant downstream component is Activity an ActivityModule graph.
     */
    IBroadcastManager broadcastManager();
    IBackgroundExecutor backgroundExecutor();
    AccountManager accountManager();
    // Watches after leaked memory, and kills what should not be alive
    Lazy<RefWatcher> refWatcher();
}