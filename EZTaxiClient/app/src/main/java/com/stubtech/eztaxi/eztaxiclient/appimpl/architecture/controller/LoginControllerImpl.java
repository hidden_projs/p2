package com.stubtech.eztaxi.eztaxiclient.appimpl.architecture.controller;

import android.accounts.AccountManager;
import android.content.Context;
import android.support.annotation.NonNull;
import android.widget.Toast;

import com.google.common.base.Preconditions;
import com.stubtech.eztaxi.eztaxiclient.appabstract.authentication.UserCredentials;
import com.stubtech.eztaxi.eztaxiclient.appanotation.ui.ActivityScope;
import com.stubtech.eztaxi.eztaxiclient.appimpl.service.AuthService;
import com.stubtech.eztaxi.eztaxiclient.appinterface.architecture.controller.ILoginController;
import com.stubtech.eztaxi.eztaxiclient.appinterface.architecture.view.ILoginView;
import com.stubtech.eztaxi.eztaxiclient.appinterface.broadcast.IBroadcastManager;

import javax.inject.Inject;

@ActivityScope
public class LoginControllerImpl implements ILoginController {

    private final ILoginView view;
    private final AccountManager accountManager;
    private final IBroadcastManager broadcastManager;

    @Inject
    public LoginControllerImpl(@NonNull ILoginView view,
                               @NonNull AccountManager accountManager,
                               @NonNull IBroadcastManager broadcastManager) {
        Preconditions.checkNotNull(view);
        Preconditions.checkNotNull(accountManager);
        Preconditions.checkNotNull(broadcastManager);
        this.view = view;
        this.accountManager = accountManager;
        this.broadcastManager = broadcastManager;
    }

    public void requestAccess() {
        if (!validateUserInput()) {
            return;
        }

        //---------------------------------------------onLoginFailed();

        final String un = view.getUN();
        final String pw = view.getPW();
        final String tokenType = view.getAuthTokenType();
        final UserCredentials credentials = UserCredentials.builder()
                .setUN(un)
                .setPW(pw)
                .setTokenType(tokenType)
                .build();
        broadcastManager.register(this);
        AuthService.onLogin((Context) view, credentials);
    }

    @Override
    public void destroy() {
        broadcastManager.unregister(this);
    }

    public boolean validateUserInput() {
        boolean valid = true;
        String email = view.getUN();
        String password = view.getPW();
        if (email.isEmpty() || !android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            view.getmTilUN().setError("enter a valid email address");
            valid = false;
        } else {
            view.getmTilUN().setError(null);
        }
        if (password.isEmpty() || password.length() < 4 || password.length() > 10) {
            view.getmTilPW().setError("between 4 and 10 alphanumeric characters");
            valid = false;
        } else {
            view.getmTilPW().setError(null);
        }
        return valid;
    }
}
