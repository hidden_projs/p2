package com.stubtech.eztaxi.eztaxiclient;

import android.app.Application;
import android.content.Context;
import android.util.Log;

import com.stubtech.eztaxi.eztaxiclient.appimpl.dependencyInjection.DependencyInjector;
import com.stubtech.eztaxi.eztaxiclient.appimpl.dependencyInjection.module.ApplicationModule;
import com.stubtech.eztaxi.eztaxiclient.appimpl.dependencyInjection.module.AuthenticationModule;
import com.stubtech.eztaxi.eztaxiclient.appimpl.dependencyInjection.module.BroadcastModule;
import com.stubtech.eztaxi.eztaxiclient.appimpl.dependencyInjection.module.HttpModule;
import com.stubtech.eztaxi.eztaxiclient.appimpl.dependencyInjection.module
        .MemoryLeakDetectionModule;
import com.stubtech.eztaxi.eztaxiclient.appinterface.dependencyinjection.IModuleSupplier;
import com.stubtech.eztaxi.eztaxiclient.apputil.Constants;
import com.stubtech.eztaxi.eztaxiclient.apputil.FacebookDebugBridge;

/**
 * Application class itself.
 * <p/>
 * Instantiation is necessary for "bootstrap injection", so I can use Dependency Graph for Dagger
 */
public class EZTaxiClientApplication extends Application implements IModuleSupplier {

    @Override
    public void onCreate() {
        // @First method to be called in Application lifecycle
        super.onCreate();
        FacebookDebugBridge.installStetho(this);
        Log.d(Constants.DEBUG_TAG, "Initialised Application");
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        DependencyInjector.initialize(this);
        Log.d(Constants.DEBUG_TAG, "Injected Application");
    }

    @Override
    public ApplicationModule provideApplicationModule() {
        return new ApplicationModule(this);
    }

    @Override
    public AuthenticationModule provideAuthenticationModule() {
        return new AuthenticationModule();
    }

    @Override
    public BroadcastModule provideBroadcastModule() {
        return new BroadcastModule();
    }

    @Override
    public HttpModule provideHttpModule() {
        return new HttpModule();
    }

    @Override
    public MemoryLeakDetectionModule provideMemoryLeakDetectionModule() {
        return new MemoryLeakDetectionModule(BuildConfig.DEBUG, this);
    }
}
