package com.stubtech.eztaxi.eztaxiclient.old;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.stubtech.eztaxi.eztaxiclient.appimpl.architecture.view.HomeActivity;
import com.stubtech.eztaxi.eztaxiclient.appimpl.architecture.view.LoginActivity;
import com.stubtech.eztaxi.eztaxiclient.apputil.Constants;

public class AppAuthentificator {
    public static void setLoginState(Context context, boolean state) {
        try {
            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
            SharedPreferences.Editor editor = prefs.edit();
            editor.putBoolean(Constants.SecurityConstants.LOGIN_STATE, state);
            editor.commit();
        } catch (Exception e) {
            Log.e(Constants.DEBUG_TAG, e.getMessage());
            return;
        }
    }

    public static boolean getLoggedInState(Context context) {
        try {
            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
            return prefs.getBoolean(Constants.SecurityConstants.LOGIN_STATE, false);
        } catch (Exception e) {
            Log.e(Constants.DEBUG_TAG, e.getMessage());
            return false;
        }
    }

    public static void setXAuthToken(Context context, String token) {
        try {
            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
            SharedPreferences.Editor editor = prefs.edit();
            editor.putString(Constants.X_AUTH_TOKEN, token);
            editor.commit();
        } catch (Exception e) {
            Log.e(Constants.DEBUG_TAG, e.getMessage());
            return;
        }
    }


    public static String getXAuthToken(Context context) {
        try {
            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
            return prefs.getString(Constants.X_AUTH_TOKEN, "");
        } catch (Exception e) {
            Log.e(Constants.DEBUG_TAG, e.getMessage());
            return "";
        }
    }

    public static void reRouteUnAuthenticated(AppCompatActivity activity) {
        try {
            Intent intent;
            boolean logged_in_check = AppAuthentificator.getLoggedInState(activity
                    .getApplicationContext());
            if (logged_in_check) {
                intent = new Intent(activity, HomeActivity.class);
                activity.startActivity(intent);
                activity.finishActivity(0);
                Log.d(Constants.DEBUG_TAG, "reRouted Authenticated");
                return;
            } else {
                intent = new Intent(activity, LoginActivity.class);
                activity.startActivity(intent);
                activity.finishActivity(0);
                Log.d(Constants.DEBUG_TAG, "reRouted unAuthenticated from: " + activity.getComponentName());
                return;
            }
        } catch (Exception e) {
            Log.e(Constants.DEBUG_TAG, e.getMessage());
        }
    }

    public static void routeAuthenticated(AppCompatActivity activity) {
        try {
            android.os.Debug.waitForDebugger();
            Intent intent;
            boolean logged_in_check = AppAuthentificator.getLoggedInState(activity
                    .getApplicationContext());
            if (logged_in_check) {
                intent = new Intent(activity, HomeActivity.class);
                activity.startActivity(intent);
                activity.finishActivity(0);
                Log.d(Constants.DEBUG_TAG, "Routed Authenticated");
                return;
            }
        } catch (Exception e) {
            Log.e(Constants.DEBUG_TAG, e.getMessage());
        }
    }
}
