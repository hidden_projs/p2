package com.stubtech.eztaxi.eztaxiclient.appinterface.architecture.view;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputLayout;
import android.support.v7.widget.AppCompatButton;
import android.widget.TextView;

public interface ILoginView extends IButterKnifeSupportingView, IDaggerInjetionSupportingView {
    void onErrorOccured(@NonNull String message);
    void onUserSignedIn(@NonNull Bundle result);
    AppCompatButton getmBtnLogin();
    TextView getmRegisterLink();
    TextInputLayout getmTilUN();
    TextInputLayout getmTilPW();
    TextView getmTxtUN();
    TextView getmTxtPW();
}
