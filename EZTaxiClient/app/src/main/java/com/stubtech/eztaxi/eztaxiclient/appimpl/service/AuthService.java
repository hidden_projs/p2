package com.stubtech.eztaxi.eztaxiclient.appimpl.service;

import android.accounts.AccountManager;
import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.IntDef;
import android.support.annotation.NonNull;

import com.google.common.base.Preconditions;
import com.stubtech.eztaxi.eztaxiclient.appabstract.authentication.UserCredentials;
import com.stubtech.eztaxi.eztaxiclient.appimpl.broadcast.auth.AccountLoginBroadcastMessage;
import com.stubtech.eztaxi.eztaxiclient.appimpl.broadcast.auth.AccountRegisterBroadcastMessage;
import com.stubtech.eztaxi.eztaxiclient.appimpl.dependencyInjection.DependencyInjector;
import com.stubtech.eztaxi.eztaxiclient.appimpl.exception.AuthException;
import com.stubtech.eztaxi.eztaxiclient.appinterface.authentication.IAuthProvider;
import com.stubtech.eztaxi.eztaxiclient.appinterface.broadcast.IBroadcastManager;

import javax.inject.Inject;
import javax.inject.Named;

/**
 * Authenticator that implements communication with Android Account Manager
 */
public class AuthService extends IntentService {

    private static final String TAG = AuthService.class.getSimpleName();
    private static final String EXTRA_REQUEST = "request";
    private static final String EXTRA_CREDENTIALS = "credentials";

    @Inject
    @Named("AuthenticatorProvider")
    IAuthProvider authProviderImpl;

    @Inject
    IBroadcastManager broadcastManagerImpl;

    public AuthService() {
        super(TAG);
    }

    public static void onRegister(@NonNull Context context, @NonNull UserCredentials credentials) {
        shipWithRequest(context, credentials, AuthRequest.REQUEST_REGISTER);
    }

    public static void onLogin(@NonNull Context context, @NonNull UserCredentials credentials) {
        shipWithRequest(context, credentials, AuthRequest.REQUEST_LOGIN);
    }

    private static void shipWithRequest(@NonNull final Context context,
                                        @NonNull final UserCredentials credentials,
                                        @AuthRequest final int request) {
        final Intent intent = new Intent(context, AuthService.class).putExtra(EXTRA_REQUEST,
                request)
                .putExtra(EXTRA_CREDENTIALS, credentials);
        context.startService(intent);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        DependencyInjector.getGraph().inject(this);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        final int request = intent.getIntExtra(EXTRA_REQUEST, Integer.MAX_VALUE);
        final UserCredentials userCredentials = intent.getParcelableExtra(EXTRA_CREDENTIALS);
        switch (request) {
            case AuthRequest.REQUEST_REGISTER:
                onRegister(userCredentials);
                break;
            case AuthRequest.REQUEST_LOGIN:
                onLogin(userCredentials);
                break;
            default:
                throw new UnsupportedOperationException("Unknown request id to be handled: " +
                        request);
        }
    }

    private void onRegister(UserCredentials userCredentials) {
        final Bundle data = new Bundle();
        try {
            String authToken = authProviderImpl.register(userCredentials);
            data.putString(AccountManager.KEY_ACCOUNT_NAME, userCredentials.getUN());
            data.putString(AccountManager.KEY_ACCOUNT_TYPE, userCredentials.getAccType());
            data.putString(AccountManager.KEY_AUTHTOKEN, authToken);
            data.putString(AccountManager.KEY_PASSWORD, userCredentials.getPW());
        } catch (AuthException e) {
            data.putString(AccountManager.KEY_ERROR_MESSAGE, e.getMessage());
        }
        final AccountRegisterBroadcastMessage message = new AccountRegisterBroadcastMessage(data);
        sendData(message);
    }

    private void onLogin(UserCredentials userCredentials) {
        final Bundle data = new Bundle();
        try {
            String authToken = authProviderImpl.login(userCredentials);
            data.putString(AccountManager.KEY_ACCOUNT_NAME, userCredentials.getUN());
            data.putString(AccountManager.KEY_ACCOUNT_TYPE, userCredentials.getAccType());
            data.putString(AccountManager.KEY_AUTHTOKEN, authToken);
            data.putString(AccountManager.KEY_PASSWORD, userCredentials.getPW());
        } catch (AuthException e) {
            data.putString(AccountManager.KEY_ERROR_MESSAGE, e.getMessage());
        }
        final AccountLoginBroadcastMessage message = new AccountLoginBroadcastMessage(data);
        sendData(message);
    }

    void sendData(@NonNull Object object) {
        Preconditions.checkNotNull(object, "Post Object cannot be null.");
        broadcastManagerImpl.post(object);
    }

    @IntDef({AuthRequest.REQUEST_REGISTER, AuthRequest.REQUEST_LOGIN})
    @interface AuthRequest {

        int REQUEST_REGISTER = 1;
        int REQUEST_LOGIN = 2;
    }
}
