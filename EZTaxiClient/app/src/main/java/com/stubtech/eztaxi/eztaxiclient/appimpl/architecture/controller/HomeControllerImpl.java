package com.stubtech.eztaxi.eztaxiclient.appimpl.architecture.controller;

import android.app.Activity;
import android.support.annotation.NonNull;

import com.google.common.base.Preconditions;
import com.stubtech.eztaxi.eztaxiclient.appanotation.ui.ActivityScope;
import com.stubtech.eztaxi.eztaxiclient.appinterface.architecture.controller.IHomeController;
import com.stubtech.eztaxi.eztaxiclient.appinterface.architecture.view.IHomeView;
import com.stubtech.eztaxi.eztaxiclient.appinterface.controller.IBackgroundExecutor;

import javax.inject.Inject;

/**
 * Implementation of injectable Controller for the Home Activity
 */
@ActivityScope
public class HomeControllerImpl implements IHomeController {

    private final IHomeView view;
    private final Activity activity;
    //private final AccountManager accountManager;
    private final IBackgroundExecutor executor;

    @Inject
    public HomeControllerImpl(@NonNull IHomeView view, @NonNull Activity activity,
                              //@NonNull AccountManager accountManager,
                              @NonNull IBackgroundExecutor executor) {
        Preconditions.checkNotNull(view);
        //Preconditions.checkNotNull(accountManager);
        Preconditions.checkNotNull(executor);
        this.view = view;
        this.activity = activity;
        //this.accountManager = accountManager;
        this.executor = executor;
    }

    @Override
    public void destroy() {
    }

    @Override
    public void logout() {
    }

    @Override
    public void orderTaxi() {
    }
}
