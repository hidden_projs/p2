package com.stubtech.eztaxi.eztaxiclient.apputil;

import android.app.Application;
import android.support.annotation.NonNull;

import com.facebook.stetho.Stetho;
import com.stubtech.eztaxi.eztaxiclient.BuildConfig;

public class FacebookDebugBridge {

    /**
     * Forbid Object construction.
     */
    private FacebookDebugBridge() {
    }

    /**
     * If debugging allows nice debug tool to use via chrome browser
     * <p/>
     * From API: Stetho is a sophisticated debug bridge for Android applications. When enabled,
     * developers have access to the Chrome Developer Tools feature natively part of the Chrome
     * desktop browser. Developers can also choose to enable the optional dumpapp tool which
     * offers a powerful command-line interface to application internals.
     *
     * @param application EZTaxiClientApplication
     */
    public static void installStetho(@NonNull Application application) {
        if (BuildConfig.DEBUG) {
            Stetho.initialize(Stetho.newInitializerBuilder(application)
                    .enableDumpapp(Stetho.defaultDumperPluginsProvider(application))
                    .enableWebKitInspector(Stetho.defaultInspectorModulesProvider(application))
                    .build());
        }
    }
}