package com.stubtech.eztaxi.eztaxiclient.appinterface.architecture.view;

public interface IButterKnifeSupportingView {

    void bindViews();
}
