package com.stubtech.eztaxi.eztaxiclient.apputil;

/**
 * Class that contains constants to represent common HTTP codes
 */
public final class HttpStatusCodes {

    public static final int SC_OK = 200;
    public static final int SC_CREATED = 201;
    public static final int SC_NOT_FOUND = 404;
    public static final int SC_UNAUTHORIZED = 403;
    public static final int SC_BAD_REQUEST = 400;

    private HttpStatusCodes() {
    }
}