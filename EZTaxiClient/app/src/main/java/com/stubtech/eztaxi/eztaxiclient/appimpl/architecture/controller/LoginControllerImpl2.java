package com.stubtech.eztaxi.eztaxiclient.appimpl.architecture.controller;

import android.accounts.AccountManager;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;

import com.google.common.base.Preconditions;
import com.stubtech.eztaxi.eztaxiclient.appabstract.authentication.UserCredentials;
import com.stubtech.eztaxi.eztaxiclient.appanotation.ui.ActivityScope;
import com.stubtech.eztaxi.eztaxiclient.appimpl.service.AuthService;
import com.stubtech.eztaxi.eztaxiclient.appinterface.architecture.controller.ILoginController2;
import com.stubtech.eztaxi.eztaxiclient.appinterface.architecture.view.ILoginView2;
import com.stubtech.eztaxi.eztaxiclient.appinterface.broadcast.IBroadcastManager;

import javax.inject.Inject;

/**
 * Implementation of injectable Controller for the Login Activity
 */
@ActivityScope
public class LoginControllerImpl2 implements ILoginController2 {

    private final ILoginView2 view;
    private final AccountManager accountManager;
    private final IBroadcastManager broadcastManager;

    @Inject
    public LoginControllerImpl2(@NonNull ILoginView2 view,
                                @NonNull AccountManager accountManager,
                                @NonNull IBroadcastManager broadcastManager) {
        Preconditions.checkNotNull(view);
        Preconditions.checkNotNull(accountManager);
        Preconditions.checkNotNull(broadcastManager);
        this.view = view;
        this.accountManager = accountManager;
        this.broadcastManager = broadcastManager;
    }

    public void requestAccess() {
        final String un = view.getUsername();
        final String pw = view.getPassword();
        final String tokenType = view.getAuthTokenType();
        final UserCredentials credentials = UserCredentials.builder()
                .setUN(un)
                .setPW(pw)
                .setTokenType(tokenType)
                .build();
        broadcastManager.register(this);
        AuthService.onLogin((Context) view, credentials);
    }

    @Override
    public void login() {
    }

    @Override
    public void notifyLoggedIn(@NonNull Bundle data) {
    }

    @Override
    public void destroy() {
        broadcastManager.unregister(this);
    }
}
