package com.stubtech.eztaxi.eztaxiclient.appinterface.dependencyinjection;

/**
 * This interface provides signatures to Dagger to add all modules to the Application scoped
 * DependencyGraph
 */
public interface IMainComponent extends IDependencyGraph {

    final class Initializer {

        /**
         * Forbid Object construction.
         */
        private Initializer() {
        }

        /**
         * Explicitly build graph since modules have constructor parameter.
         *
         * @param moduleSupplier
         * @return
         */

        public static IDependencyGraph initialize(IModuleSupplier moduleSupplier) {
            return DaggerIDependencyGraph.builder()
                    .applicationModule(moduleSupplier.provideApplicationModule())
                    .authenticationModule(moduleSupplier.provideAuthenticationModule())
                    .broadcastModule(moduleSupplier.provideBroadcastModule())
                    .httpModule(moduleSupplier.provideHttpModule())
                    .memoryLeakDetectionModule(moduleSupplier.provideMemoryLeakDetectionModule())
                    .build();
        }
    }
}
