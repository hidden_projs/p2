package com.stubtech.eztaxi.eztaxiclient.appinterface.authentication;

import com.stubtech.eztaxi.eztaxiclient.appabstract.authentication.UserCredentials;
import com.stubtech.eztaxi.eztaxiclient.appimpl.exception.AuthException;
import com.stubtech.eztaxi.eztaxiclient.apputil.Destroyable;

/**
 * Interface that declares methods, that will be used with external service to perform
 * authentication
 */
public interface IAuthProvider extends Destroyable {

    String login(UserCredentials userCredentials) throws AuthException;
    String register(UserCredentials userCredentials) throws AuthException;
}
