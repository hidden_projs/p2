package com.stubtech.eztaxi.eztaxiclient.appimpl.broadcast.auth;

import android.os.Bundle;
import android.support.annotation.NonNull;

import com.stubtech.eztaxi.eztaxiclient.appabstract.broadcast.authentication
        .AbstractAuthBroadcastMessage;

/**
 * Login message wrapper to communicate with Android Account Manager
 */
public class AccountLoginBroadcastMessage extends AbstractAuthBroadcastMessage {

    public AccountLoginBroadcastMessage(@NonNull Bundle data) {
        super(data);
    }

    @Override
    public void destroy() {
    }
}
