package com.stubtech.eztaxi.eztaxiclient.appimpl.architecture.view;

import android.accounts.Account;
import android.accounts.AccountAuthenticatorActivity;
import android.accounts.AccountManager;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputLayout;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.common.api.GoogleApiClient;
import com.stubtech.eztaxi.eztaxiclient.R;
import com.stubtech.eztaxi.eztaxiclient.appabstract.activity.AbstractAppActivity;
import com.stubtech.eztaxi.eztaxiclient.appimpl.dependencyInjection.DependencyInjector;
import com.stubtech.eztaxi.eztaxiclient.appinterface.architecture.view.IButterKnifeSupportingView;
import com.stubtech.eztaxi.eztaxiclient.appinterface.architecture.view
        .IDaggerInjetionSupportingView;
import com.stubtech.eztaxi.eztaxiclient.appinterface.architecture.view.ILoginView2;
import com.stubtech.eztaxi.eztaxiclient.apputil.Constants;
import com.stubtech.eztaxi.eztaxiclient.old.ConnectionChecker;
import com.stubtech.eztaxi.eztaxiclient.old.UserAuthTask;

import butterknife.ButterKnife;

/**
 * A login screen that offers login via un/pw.
 */
public class LoginActivity2 extends AccountAuthenticatorActivity implements ILoginView2,
                                                                            IDaggerInjetionSupportingView, IButterKnifeSupportingView {

    protected Account mAccount;
    protected AccountManager mAccountManager;
    protected String mAccountName;
    protected String mAccountType;
    protected String mAuthType;
    protected String mAuthToken;
    private Button mBtnSignIn;
    private Button mBtnGoToRegister;
    private EditText mTxtPW;
    private EditText mTxtUN;
    private TextView mLoginStatusMessageView;
    private TextInputLayout mTilUN;
    private TextInputLayout mTilPW;
    private ProgressDialog mLoginProgressDialog;
    private UserAuthTask mUserAuthTask;
    private String mUN;
    private String mPW;
    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    private GoogleApiClient client;

    @Override
    public void onCreate(Bundle savedInstanceState, PersistableBundle persistentState) {
        super.onCreate(savedInstanceState, persistentState);
        prepare();
    }

    private void prepare() {
        setContentView(R.layout.activity_login);
        mAccountManager = AccountManager.get(getBaseContext());
        mAccountName = getIntent().getStringExtra(AbstractAppActivity.ARG_ACCOUNT_NAME);
        mAccountType = getIntent().getStringExtra(AbstractAppActivity.ARG_ACCOUNT_TYPE);
        mAuthType = getIntent().getStringExtra(AbstractAppActivity.ARG_AUTH_TYPE);
        // Set up the login form. Get all UI elements
        mBtnGoToRegister = (Button) findViewById(R.id.login_btn_goto_register);
        mBtnSignIn = (Button) findViewById(R.id.login_btn_login);
        mTxtUN = (EditText) findViewById(R.id.login_txt_un);
        mTxtPW = (EditText) findViewById(R.id.login_txt_pw);
        mLoginStatusMessageView = (TextView) findViewById(R.id.login_lbl_error);
        mTilUN = (TextInputLayout) findViewById(R.id.login_til_un);
        mTilPW = (TextInputLayout) findViewById(R.id.login_til_pw);
        mLoginProgressDialog = createDialog();
        mTilPW.setErrorEnabled(true);
        mTilUN.setErrorEnabled(true);
        mBtnSignIn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                attemptLogin();
            }
        });
        mBtnGoToRegister.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                gotoRegister();
            }
        });
    }

    private void attemptLogin() {
        clearValidationErrors();
        //TODO: use fields not constants
        mUN = mTxtUN.getText().toString();
        mPW = mTxtPW.getText().toString();
        mUN = "testuser";
        mPW = "testpass";
        if (checkValidationErrors()) {
            return;
        }
        if (ConnectionChecker.hasConnection(getApplicationContext())) {
            mUserAuthTask = new UserAuthTask(this);
            mLoginProgressDialog.show();
            mUserAuthTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        } else {
            Log.d(Constants.DEBUG_TAG, "No internet connection");
        }
    }

    private void clearValidationErrors() {
        mLoginStatusMessageView.setVisibility(View.INVISIBLE);
        mTilUN.setError(null);
        mTilPW.setError(null);
    }

    private boolean checkValidationErrors() {
        //TODO: Implement this
        //mTilPW.setError(R.string.username_required);
        return false;
    }

    private void gotoRegister() {
        Intent intent = new Intent(this, RegisterActivity.class);
        this.startActivity(intent);
        this.finishActivity(0);
        return;
    }

    private ProgressDialog createDialog() {
        ProgressDialog mLoginProgressDialog = new ProgressDialog(getApplicationContext(),
                ProgressDialog.STYLE_SPINNER);
        mLoginProgressDialog.setTitle("Authenticating...");
        mLoginProgressDialog.setButton(ProgressDialog.BUTTON_NEGATIVE, "CANCEL", new
                DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        cancelLogin();
                        dialog.dismiss();
                    }
                });
        return mLoginProgressDialog;
    }

    private void cancelLogin() {
        mUserAuthTask.cancel(true);
    }

    public String getmUN() {
        return mUN;
    }

    public String getmPW() {
        return mPW;
    }

    public String getmAccountName() {
        return mAccountName;
    }

    public String getmAccountType() {
        return mAccountType;
    }

    public String getmAuthType() {
        return mAuthType;
    }

    public AccountManager getmAccountManager() {
        return mAccountManager;
    }

    public TextView getmLoginStatusMessageView() {
        return mLoginStatusMessageView;
    }

    public void clearmUserAuthTask() {
        mUserAuthTask = null;
    }

    public void bindViews() {
        ButterKnife.bind(this);
    }

    public void injectMembers() {
        DependencyInjector.activityComponent(this).inject(this);
    }

    @NonNull
    @Override
    public String getUsername() {
        return null;
    }

    @NonNull
    @Override
    public String getPassword() {
        return null;
    }

    @NonNull
    @Override
    public String getAuthTokenType() {
        return null;
    }

    @Override
    public void onErrorOccured(@NonNull String message) {
    }

    @Override
    public void onUserSignedIn(@NonNull Bundle result) {
    }

    @Override
    public boolean isNewAccountRequested() {
        return false;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();
    }

    @Override
    public void onStart() {
        super.onStart();
        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client.connect();
        Action viewAction = Action.newAction(Action.TYPE_VIEW, // TODO: choose an action type.
                "Login Page", // TODO: Define a title for the content shown.
                // TODO: If you have web page content that matches this app activity's content,
                // make sure this auto-generated web page URL is correct.
                // Otherwise, set the URL to null.
                Uri.parse("http://host/path"),
                // TODO: Make sure this auto-generated app deep link URI is correct.
                Uri.parse("android-app://com.stubtech.eztaxi.eztaxiclient.appimpl.architecture" +
                        ".view/http/host/path"));
        AppIndex.AppIndexApi.start(client, viewAction);
    }

    @Override
    public void onStop() {
        super.onStop();
        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        Action viewAction = Action.newAction(Action.TYPE_VIEW, // TODO: choose an action type.
                "Login Page", // TODO: Define a title for the content shown.
                // TODO: If you have web page content that matches this app activity's content,
                // make sure this auto-generated web page URL is correct.
                // Otherwise, set the URL to null.
                Uri.parse("http://host/path"),
                // TODO: Make sure this auto-generated app deep link URI is correct.
                Uri.parse("android-app://com.stubtech.eztaxi.eztaxiclient.appimpl.architecture" +
                        ".view/http/host/path"));
        AppIndex.AppIndexApi.end(client, viewAction);
        client.disconnect();
    }
}

