package com.stubtech.eztaxi.eztaxiclient.appinterface.architecture.view;

import android.accounts.Account;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

/**
 * Activity that interacts with IAccountManagerHelper
 * might need to have view that partially implements this interface
 */
public interface IAccountHandlerView {

    void onAccountAdded();
    void onNoAccountsCreated();
    void onErrorOccurred(@NonNull String message);
    void onRequestedTokenObtained(@Nullable String authToken);
    void onAccountRemoved(@NonNull Account account);
    void onAuthTokenLoaded(@NonNull String authtoken);
    void onAuthTokenInvalidated(@NonNull Account account);
}
