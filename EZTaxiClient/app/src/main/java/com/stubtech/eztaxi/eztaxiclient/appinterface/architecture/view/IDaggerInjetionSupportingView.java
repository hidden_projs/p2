package com.stubtech.eztaxi.eztaxiclient.appinterface.architecture.view;

public interface IDaggerInjetionSupportingView {

    void injectMembers();
}
