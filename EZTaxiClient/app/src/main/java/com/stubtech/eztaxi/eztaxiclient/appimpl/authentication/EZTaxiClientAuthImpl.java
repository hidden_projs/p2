package com.stubtech.eztaxi.eztaxiclient.appimpl.authentication;

import android.accounts.AbstractAccountAuthenticator;
import android.accounts.Account;
import android.accounts.AccountAuthenticatorResponse;
import android.accounts.AccountManager;
import android.accounts.NetworkErrorException;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;

import com.google.common.base.Objects;
import com.stubtech.eztaxi.eztaxiclient.R;
import com.stubtech.eztaxi.eztaxiclient.appabstract.authentication.UserCredentials;
import com.stubtech.eztaxi.eztaxiclient.appimpl.architecture.view.LoginActivity;
import com.stubtech.eztaxi.eztaxiclient.appimpl.dependencyInjection.DependencyInjector;
import com.stubtech.eztaxi.eztaxiclient.appimpl.exception.AuthException;
import com.stubtech.eztaxi.eztaxiclient.appinterface.authentication.IAuthProvider;
import com.stubtech.eztaxi.eztaxiclient.apputil.Constants;

import javax.inject.Inject;
import javax.inject.Named;

import autovalue.shaded.com.google.common.common.base.Joiner;

/**
 * Implementation of Android Account Authenticator, to use android Account Manager.
 * At least allow to programmatically add an account to manager
 * and perform authentication with RestApi Service
 */
public class EZTaxiClientAuthImpl extends AbstractAccountAuthenticator {

    private final Context mContext;
    @Inject
    AccountManager accountManager;

    @Inject
    @Named("AuthenticatorProvider")
    IAuthProvider authProviderImpl;

    public EZTaxiClientAuthImpl(Context context) {
        super(context);
        DependencyInjector.getGraph().inject(this);
        this.mContext = context;
    }

    @Override
    public Bundle addAccount(AccountAuthenticatorResponse response,
                             String accountType,
                             String authTokenType,
                             String[] requiredFeatures,
                             Bundle options) throws NetworkErrorException {
        final Intent intent = new Intent(mContext, LoginActivity.class).putExtra(Constants
                .ACC_TYPE, accountType)
                .putExtra(Constants.X_AUTH_TOKEN, authTokenType)
                .putExtra(Constants.IS_NEW_ACC, true)
                .putExtra(AccountManager.KEY_ACCOUNT_AUTHENTICATOR_RESPONSE, response);
        final Bundle bundle = new Bundle();
        bundle.putParcelable(AccountManager.KEY_INTENT, intent);
        return bundle;
    }

    @Override
    public Bundle confirmCredentials(AccountAuthenticatorResponse response,
                                     Account account,
                                     Bundle options) throws NetworkErrorException {
        return null;
    }

    @Override
    public Bundle editProperties(AccountAuthenticatorResponse response, String accountType) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Bundle getAuthToken(AccountAuthenticatorResponse response,
                               Account account,
                               String authTokenType,
                               Bundle options) throws NetworkErrorException {
        if (!Objects.equal(Constants.Access.AUTH_TOKEN_TYPE_READ_ONLY, authTokenType) && !Objects
                .equal(Constants.Access.AUTH_TOKEN_FULL_ACCESS, authTokenType)) {
            final Bundle result = new Bundle();
            result.putString(AccountManager.KEY_ERROR_MESSAGE, mContext.getString(R.string
                    .invalid_auth_token));
            return result;
        }
        String authenticationToken = accountManager.peekAuthToken(account, authTokenType);
        if (TextUtils.isEmpty(authenticationToken)) {
            final String password = accountManager.getPassword(account);
            if (!TextUtils.isEmpty(password)) {
                try {
                    authenticationToken = authProviderImpl.login(UserCredentials.builder()
                            .setUN(account.name)
                            .setPW(password)
                            .setTokenType(authTokenType)
                            .build());
                } catch (AuthException e) {
                    e.printStackTrace();
                }
            }
        }
        if (!TextUtils.isEmpty(authenticationToken)) {
            final Bundle result = new Bundle();
            result.putString(AccountManager.KEY_ACCOUNT_NAME, account.name);
            result.putString(AccountManager.KEY_ACCOUNT_TYPE, account.type);
            result.putString(AccountManager.KEY_AUTHTOKEN, authenticationToken);
            return result;
        }
        final Intent intent = new Intent(mContext, LoginActivity2.class).putExtra(AccountManager
                .KEY_ACCOUNT_AUTHENTICATOR_RESPONSE, response)
                .putExtra(Constants.ACC_TYPE, account.type)
                .putExtra(Constants.X_AUTH_TOKEN, authTokenType)
                .putExtra(Constants.ACC_NAME, account.name);
        final Bundle bundle = new Bundle();
        bundle.putParcelable(AccountManager.KEY_INTENT, intent);
        return bundle;
    }

    @Override
    public String getAuthTokenLabel(String authTokenType) {
        if (Constants.Access.AUTH_TOKEN_FULL_ACCESS.equals(authTokenType)) {
            return Constants.Access.AUTH_TOKEN_FULL_ACCESS_LABEL;
        } else if (Constants.Access.AUTH_TOKEN_TYPE_READ_ONLY.equals(authTokenType)) {
            return Constants.Access.AUTH_TOKEN_TYPE_READ_ONLY_LABEL;
        } else {
            return Joiner.on("").join(authTokenType, "(Label)");
        }
    }

    @Override
    public Bundle updateCredentials(AccountAuthenticatorResponse response,
                                    Account account,
                                    String authTokenType,
                                    Bundle options) throws NetworkErrorException {
        return null;
    }

    @Override
    public Bundle hasFeatures(AccountAuthenticatorResponse response,
                              Account account,
                              String[] features) throws NetworkErrorException {
        final Bundle result = new Bundle();
        result.putBoolean(AccountManager.KEY_BOOLEAN_RESULT, false);
        return result;
    }
}
