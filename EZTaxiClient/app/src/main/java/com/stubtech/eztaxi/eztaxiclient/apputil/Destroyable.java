package com.stubtech.eztaxi.eztaxiclient.apputil;

/**
 * Self explanatory interface
 */
public interface Destroyable {

    void destroy();
}
