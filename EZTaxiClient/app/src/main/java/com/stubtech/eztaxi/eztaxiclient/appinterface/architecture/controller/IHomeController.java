package com.stubtech.eztaxi.eztaxiclient.appinterface.architecture.controller;

import com.stubtech.eztaxi.eztaxiclient.apputil.Destroyable;

/**
 * Contract for Home Activity Controller
 */
public interface IHomeController extends Destroyable {
    void logout();
    void orderTaxi();
}
