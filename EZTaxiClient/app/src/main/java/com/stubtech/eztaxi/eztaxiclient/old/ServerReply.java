package com.stubtech.eztaxi.eztaxiclient.old;

public class ServerReply {
    private ServerReplyStatus replyStatus;
    private String info;

    public ServerReply(ServerReplyStatus replyStatus, String info) {
        this.replyStatus = replyStatus;
        this.info = info;
    }

    public ServerReplyStatus getReplyStatus() {
        return replyStatus;
    }

    public void setReplyStatus(ServerReplyStatus replyStatus) {
        this.replyStatus = replyStatus;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }
}
