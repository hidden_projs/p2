package com.stubtech.eztaxi.eztaxiclient.appimpl.dependencyInjection;

import android.app.Activity;
import android.support.annotation.NonNull;

import com.google.common.base.Preconditions;
import com.squareup.leakcanary.RefWatcher;
import com.stubtech.eztaxi.eztaxiclient.appimpl.dependencyInjection.module.ActivityModule;
import com.stubtech.eztaxi.eztaxiclient.appinterface.dependencyinjection.DaggerIActivityComponent;
import com.stubtech.eztaxi.eztaxiclient.appinterface.dependencyinjection.IActivityComponent;
import com.stubtech.eztaxi.eztaxiclient.appinterface.dependencyinjection.IDependencyGraph;
import com.stubtech.eztaxi.eztaxiclient.appinterface.dependencyinjection.IMainComponent;
import com.stubtech.eztaxi.eztaxiclient.appinterface.dependencyinjection.IModuleSupplier;

/**
 * Dagger key class
 */
public class DependencyInjector {

    private static final String PRECONDITION_MESSAGE_GRAPH_NOT_INITIALIZED = "Dependency graph "
            + "not initialized";
    private static IDependencyGraph mainComponent;

    /**
     * Forbid Object construction.
     */
    private DependencyInjector() {
    }

    public static synchronized void initialize(IModuleSupplier supplier) {
        mainComponent = IMainComponent.Initializer.initialize(supplier);
    }

    public static synchronized boolean isInitialized() {
        return mainComponent != null;
    }

    public static synchronized IDependencyGraph getGraph() {
        Preconditions.checkState(isInitialized(), PRECONDITION_MESSAGE_GRAPH_NOT_INITIALIZED);
        return mainComponent;
    }

    /**
     * Method creates scoped graph of injectable dependencies per activity
     * Whenever new activity starts, Activity module is replaced for that activity
     */
    public static IActivityComponent activityComponent(@NonNull final Activity activity) {
        return DaggerIActivityComponent.builder()
                .iDependencyGraph(getGraph())
                .activityModule(new ActivityModule(activity))
                .build();
    }

    /**
     * Inject RefWatcher to kill objects that leak memory
     *
     * @return
     */
    public static RefWatcher refWatcher() {
        return getGraph().refWatcher().get();
    }
}
