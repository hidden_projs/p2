package com.stubtech.eztaxi.eztaxiclient.appinterface.dependencyinjection;

import com.stubtech.eztaxi.eztaxiclient.appimpl.dependencyInjection.module.ApplicationModule;
import com.stubtech.eztaxi.eztaxiclient.appimpl.dependencyInjection.module.AuthenticationModule;
import com.stubtech.eztaxi.eztaxiclient.appimpl.dependencyInjection.module.BroadcastModule;
import com.stubtech.eztaxi.eztaxiclient.appimpl.dependencyInjection.module.HttpModule;
import com.stubtech.eztaxi.eztaxiclient.appimpl.dependencyInjection.module
        .MemoryLeakDetectionModule;

/**
 * Interface that lists all modules to use as singletons for app. Subclasses can override this
 * method to
 * provide additional modules provided they call {@code super.getModules()}.
 */
public interface IModuleSupplier {

    ApplicationModule provideApplicationModule();
    AuthenticationModule provideAuthenticationModule();
    BroadcastModule provideBroadcastModule();
    HttpModule provideHttpModule();
    MemoryLeakDetectionModule provideMemoryLeakDetectionModule();
}
