package com.stubtech.eztaxi.eztaxiclient.apputil;

import android.content.Context;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.widget.Toast;

/**
 * Created by Batousik on 02.03.2016.
 */
public class Utils {

    private Utils() {
    }

    public static void showToast(final @NonNull Context context, final @NonNull String message) {
        Toast.makeText(context, message, Toast.LENGTH_LONG).show();
    }

    public static boolean isThisAMainThread() {
        return Looper.getMainLooper() == Looper.myLooper();
    }
}
