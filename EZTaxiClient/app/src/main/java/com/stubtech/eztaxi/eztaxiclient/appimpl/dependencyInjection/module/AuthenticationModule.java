package com.stubtech.eztaxi.eztaxiclient.appimpl.dependencyInjection.module;

import com.squareup.okhttp.OkHttpClient;
import com.stubtech.eztaxi.eztaxiclient.appimpl.authentication.AccountManagerHelperImpl;
import com.stubtech.eztaxi.eztaxiclient.appimpl.authentication.AuthProviderImpl;
import com.stubtech.eztaxi.eztaxiclient.appinterface.authentication.IAccountManagerHelper;
import com.stubtech.eztaxi.eztaxiclient.appinterface.authentication.IAuthProvider;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * This module provides:
 * <p/>
 * Custom Authenticator Service
 */
@Module
public class AuthenticationModule {

    @Provides
    @Singleton
    @Named("AuthenticatorProvider")
    public IAuthProvider provideAuthenticationManager(OkHttpClient okHttpClient) {
        return new AuthProviderImpl(okHttpClient);
    }

    @Provides
    @Singleton
    public IAccountManagerHelper provideAccountManagerHelper() {
        return new AccountManagerHelperImpl();
    }
}
