package com.stubtech.eztaxi.eztaxiclient.old;

import com.stubtech.eztaxi.eztaxiclient.apputil.Constants;

import java.io.BufferedWriter;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by Batousik on 23.02.2016.
 */
public class ConnectionFactory {


    public static HttpURLConnection createConnection(String requestPath, String token, String params) throws Exception {
        // Setup connection
        URL url;
        HttpURLConnection conn;
        android.os.Debug.waitForDebugger();
        url = new URL(MyAuthentificator.endpoint + requestPath);
        conn = (HttpURLConnection) url.openConnection();
        conn.setReadTimeout(999999);
        conn.setConnectTimeout(4000);
        conn.setRequestMethod("POST");
        conn.setDoInput(true);
        conn.setDoOutput(true);
        if (token != "")
            conn.setRequestProperty(Constants.X_AUTH_TOKEN, token);

        // Add params
        OutputStream os = conn.getOutputStream();
        BufferedWriter writer = new BufferedWriter(
                new OutputStreamWriter(os, "UTF-8"));
        writer.write(params);
        writer.flush();
        writer.close();
        os.close();

        return conn;
    }

////    public static HttpsURLConnection createSSLConnection(String url) throws Exception {
////        try {
////
////
////            HttpsURLConnection httpsURLConnection = new HttpsURLConnection(new URL(url)) {
////                @Override
////                public String getCipherSuite() {
////                    return null;
////                }
////
////                @Override
////                public Certificate[] getLocalCertificates() {
////                    return new Certificate[0];
////                }
////
////                @Override
////                public Certificate[] getServerCertificates() throws SSLPeerUnverifiedException {
////                    return new Certificate[0];
////                }
////
////                @Override
////                public void disconnect() {
////
////                }
////
////                @Override
////                public boolean usingProxy() {
////                    return false;
////                }
////
////                @Override
////                public void connect() throws IOException {
////
////                }
////            };
//
//            return httpsURLConnection;
//        } catch (MalformedURLException e) {
//            Log.e(Constants.DEBUG_TAG, e.getMessage());
//            throw new Exception(e);
//        }
//    }
}
