package com.stubtech.eztaxi.eztaxiclient.appinterface.architecture.controller;

import android.os.Bundle;
import android.support.annotation.NonNull;

import com.stubtech.eztaxi.eztaxiclient.appinterface.architecture.view.IButterKnifeSupportingView;
import com.stubtech.eztaxi.eztaxiclient.appinterface.architecture.view
        .IDaggerInjetionSupportingView;
import com.stubtech.eztaxi.eztaxiclient.apputil.Destroyable;

/**
 * Created by Batousik on 02.03.2016.
 */
public interface ILoginController extends Destroyable {
    void requestAccess();
    void notifyUserSignedUp(@NonNull Bundle data);
    boolean validateUserInput();
}
