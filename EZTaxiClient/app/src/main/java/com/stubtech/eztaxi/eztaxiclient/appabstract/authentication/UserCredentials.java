package com.stubtech.eztaxi.eztaxiclient.appabstract.authentication;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.Nullable;

import com.google.auto.value.AutoValue;

/**
 * Interesting tool - AutoValue, Hard to adapt, but later this is an amazing code generator tool
 * for POJOs or when you have too much mechanical, repetitive, typically untested
 * and bugged code.
 */
@AutoValue
public abstract class UserCredentials implements Parcelable {

    private static final String PARCELABLE_UN = "un";
    private static final String PARCELABLE_PW = "pw";
    private static final String PARCELABLE_EMAIL = "email";
    private static final String PARCELABLE_ACC_TYPE = "acc_type";
    private static final String PARCELABLE_TOKEN_TYPE = "token_type";
    public static final Creator<UserCredentials> CREATOR = new Creator<UserCredentials>() {
        @Override
        public UserCredentials createFromParcel(Parcel source) {
            final Bundle bundle = source.readBundle(UserCredentials.class.getClassLoader());
            return builder().setAccType(bundle.getString(PARCELABLE_ACC_TYPE))
                    .setTokenType(bundle.getString(PARCELABLE_TOKEN_TYPE))
                    .setEmail(bundle.getString(PARCELABLE_EMAIL))
                    .setUN(bundle.getString(PARCELABLE_UN))
                    .setPW(bundle.getString(PARCELABLE_PW))
                    .build();
        }

        @Override
        public UserCredentials[] newArray(int size) {
            return new UserCredentials[size];
        }
    };

    public static Builder builder() {
        return new AutoValue_UserCredentials.Builder().setAccType(PARCELABLE_ACC_TYPE);
    }

    public abstract String getUN();
    public abstract String getPW();
    @Nullable
    public abstract String getEmail();
    @Nullable
    public abstract String getAccType();
    @Nullable
    public abstract String getTokenType();

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        Bundle bundle = new Bundle(UserCredentials.class.getClassLoader());
        bundle.putString(PARCELABLE_EMAIL, getEmail());
        bundle.putString(PARCELABLE_PW, getPW());
        bundle.putString(PARCELABLE_UN, getUN());
        bundle.putString(PARCELABLE_TOKEN_TYPE, getTokenType());
        bundle.putString(PARCELABLE_ACC_TYPE, getAccType());
        dest.writeBundle(bundle);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @AutoValue.Builder
    public static abstract class Builder {

        public abstract Builder setUN(String un);
        public abstract Builder setPW(String pw);
        public abstract Builder setEmail(@Nullable String email);
        public abstract Builder setAccType(@Nullable String accType);
        public abstract Builder setTokenType(@Nullable String tokenType);
        public abstract UserCredentials build();
    }
}
