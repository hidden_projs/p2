package com.stubtech.eztaxi.eztaxiclient.appinterface.authentication;

import android.accounts.Account;
import android.app.Activity;
import android.content.DialogInterface;

import com.stubtech.eztaxi.eztaxiclient.appinterface.architecture.view.IAccountHandlerView;
import com.stubtech.eztaxi.eztaxiclient.apputil.Destroyable;

import java.util.List;

/**
 * Helper Interface for account managing
 */
public interface IAccountManagerHelper extends Destroyable {

    List<String> getApplicationAccountNames();
    Account getRegisteredAccount(int index);
    Account[] getAccountsByType();
    void addNewAccount(String access, IAccountHandlerView view, Activity activity);
    void getAuthenticationTokenByFeatures(String access,
                                          IAccountHandlerView view,
                                          Activity activity);

    void removeAccount(int which, IAccountHandlerView view);
    void loadExistingAccountAuthToken(int which,
                                      String access,
                                      Activity activity,
                                      IAccountHandlerView view);
    void invalidateAuthToken(int which, String access, Activity activity, IAccountHandlerView view);
    void showAccountsPicker(DialogInterface.OnClickListener clickListener,
                            IAccountHandlerView view,
                            Activity activity);
}
