package com.stubtech.eztaxi.eztaxiclient.appabstract.activity;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.accounts.AccountManagerCallback;
import android.accounts.AccountManagerFuture;
import android.accounts.AuthenticatorException;
import android.accounts.OperationCanceledException;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.PersistableBundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.stubtech.eztaxi.eztaxiclient.R;

import java.io.IOException;

/**
 * TODO: remove - old implementation
 */
public abstract class AbstractAppActivity extends AppCompatActivity {
    public static final String ARG_ACCOUNT_TYPE = "ACCOUNT_TYPE";
    public static final String ARG_AUTH_TYPE = "AUTH_TYPE";
    public static final String ARG_IS_ADDING_NEW_ACCOUNT = "IS_NEW_ACCOUNT_FLAG";
    public static final String ARG_ACCOUNT_NAME = "ACCOUNT_NAME";
    public static final String PARAM_USER_PASS = "USER_PW";
    private static final int ACCOUNT_CHOOSER_ACTIVITY = 1;

    protected Account mAccount;
    protected AccountManager mAccountManager;
    protected String mAccountName;
    protected String mAccountType;
    protected String mAuthType;
    protected String mAuthToken;

    @Override
    public void onCreate(Bundle savedInstanceState, PersistableBundle persistentState) {
        super.onCreate(savedInstanceState, persistentState);
        android.os.Debug.waitForDebugger();
        mAccountType = getString(R.string.authenticator_parameter_acc_type);
        mAccountManager = AccountManager.get(this);

        Account[] acc = mAccountManager.getAccountsByType(mAccountType);
        if (acc.length == 0) {
            Log.e(null, "No accounts of type " + mAccountType + " found");
            mAccountManager.addAccount(getString(R.string.authenticator_parameter_acc_type),
                    getString(R.string.auth_token_type_default),
                    null,
                    new Bundle(),
                    this,
                    new OnAccountAddComplete(),
                    null);
        } else {
            Log.i("main", "Found " + acc.length + " accounts of type " + mAccountType);
            Intent intent = AccountManager.newChooseAccountIntent(null, null, new
                            String[]{getString(R.string.authenticator_parameter_acc_type)},
                    false,
                    null,
                    getString(R.string.auth_token_type_default),
                    null,
                    null);
            startActivityForResult(intent, ACCOUNT_CHOOSER_ACTIVITY);
        }
        mAccount = acc[0];
        startAuthTokenFetch();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        android.os.Debug.waitForDebugger();
        if (resultCode == RESULT_CANCELED)
            return;
        if (requestCode == ACCOUNT_CHOOSER_ACTIVITY) {
            Bundle bundle = data.getExtras();
            mAccount = new Account(
                    bundle.getString(AccountManager.KEY_ACCOUNT_NAME),
                    bundle.getString(AccountManager.KEY_ACCOUNT_TYPE)
            );
            Log.d("main", "Selected account " + mAccount.name + ", fetching");
            startAuthTokenFetch();
        }
    }

    private void startAuthTokenFetch() {
        android.os.Debug.waitForDebugger();
        Bundle options = new Bundle();
        mAccountManager.getAuthToken(
                mAccount,
                mAuthType,
                options,
                this,
                new OnAccountManagerComplete(),
                getAlertHandler()
        );
    }

    protected abstract Handler getAlertHandler();

    private class OnAccountManagerComplete implements AccountManagerCallback<Bundle> {
        @Override
        public void run(AccountManagerFuture<Bundle> result) {
            android.os.Debug.waitForDebugger();
            Bundle bundle;
            try {
                bundle = result.getResult();
            } catch (OperationCanceledException e) {
                e.printStackTrace();
                return;
            } catch (AuthenticatorException e) {
                e.printStackTrace();
                return;
            } catch (IOException e) {
                e.printStackTrace();
                return;
            }
            mAuthToken = bundle.getString(AccountManager.KEY_AUTHTOKEN);
            Log.d("main", "Received authentication token " + mAuthToken);
        }
    }

    private class OnAccountAddComplete implements AccountManagerCallback<Bundle> {
        @Override
        public void run(AccountManagerFuture<Bundle> result) {
            android.os.Debug.waitForDebugger();
            Bundle bundle;
            try {
                bundle = result.getResult();
            } catch (OperationCanceledException e) {
                e.printStackTrace();
                return;
            } catch (AuthenticatorException e) {
                e.printStackTrace();
                return;
            } catch (IOException e) {
                e.printStackTrace();
                return;
            }
            mAccount = new Account(
                    bundle.getString(AccountManager.KEY_ACCOUNT_NAME),
                    bundle.getString(AccountManager.KEY_ACCOUNT_TYPE)
            );
            Log.d("main", "Added account " + mAccount.name + ", fetching");
            startAuthTokenFetch();
        }
    }
}
