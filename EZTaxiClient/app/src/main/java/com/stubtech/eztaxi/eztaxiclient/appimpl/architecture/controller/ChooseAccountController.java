package com.stubtech.eztaxi.eztaxiclient.appimpl.architecture.controller;

import com.stubtech.eztaxi.eztaxiclient.appanotation.ui.ActivityScope;
import com.stubtech.eztaxi.eztaxiclient.appinterface.architecture.controller
        .IChooseAccountController;

import javax.inject.Inject;

@ActivityScope
public class ChooseAccountController implements IChooseAccountController {

    @Inject
    public ChooseAccountController() {
    }
}
