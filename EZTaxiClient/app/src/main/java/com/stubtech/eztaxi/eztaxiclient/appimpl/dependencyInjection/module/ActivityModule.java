package com.stubtech.eztaxi.eztaxiclient.appimpl.dependencyInjection.module;

import android.app.Activity;
import android.os.Bundle;

import com.stubtech.eztaxi.eztaxiclient.appanotation.ui.ActivityScope;
import com.stubtech.eztaxi.eztaxiclient.appinterface.architecture.view.IChooseAccountView;
import com.stubtech.eztaxi.eztaxiclient.appinterface.architecture.view.IHomeView;
import com.stubtech.eztaxi.eztaxiclient.appinterface.architecture.view.ILoginView;
import com.stubtech.eztaxi.eztaxiclient.appinterface.architecture.view.ILoginView2;
import com.stubtech.eztaxi.eztaxiclient.appinterface.architecture.view.IRegisterView;

import dagger.Module;
import dagger.Provides;

/**
 * This module injects Activities to Dagger
 */
@Module
public class ActivityModule {

    private final Activity activity;

    public ActivityModule(Activity activity) {
        this.activity = activity;
    }

    @Provides
    @ActivityScope
    Activity provideActivity() {
        return activity;
    }

    @Provides
    @ActivityScope
    Bundle provideExtras() {
        return activity.getIntent().getExtras();
    }

    @Provides
    @ActivityScope
    IHomeView provideHomeView() {
        return (IHomeView) activity;
    }

    @Provides
    @ActivityScope
    ILoginView2 provideLoginView() {
        return (ILoginView2) activity;
    }

    @Provides
    @ActivityScope
    IRegisterView provideRegisterView() {
        return (IRegisterView) activity;
    }

    @Provides
    @ActivityScope
    IChooseAccountView provideChooseAccountView() {
        return (IChooseAccountView) activity;
    }

    @Provides
    @ActivityScope
    ILoginView provideAppStartView() {
        return (ILoginView) activity;
    }
}
