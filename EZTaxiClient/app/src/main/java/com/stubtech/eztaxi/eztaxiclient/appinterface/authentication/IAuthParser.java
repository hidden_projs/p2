package com.stubtech.eztaxi.eztaxiclient.appinterface.authentication;

import com.squareup.okhttp.RequestBody;
import com.stubtech.eztaxi.eztaxiclient.appimpl.architecture.model.ResponseParser;

import retrofit.Call;
import retrofit.http.Body;
import retrofit.http.POST;

/**
 * Interface to be used with retrofit for simple rest api calls
 * Specific for External service authentication
 */
public interface IAuthParser {

    @POST("/auth/login")
    Call<ResponseParser> login(@Body RequestBody requestBody);
    @POST("/auth/register")
    Call<ResponseParser> register(@Body RequestBody requestBody);
}
