package com.stubtech.eztaxi.eztaxiclient.appimpl.broadcast.auth;

import android.os.Bundle;
import android.support.annotation.NonNull;

import com.stubtech.eztaxi.eztaxiclient.appabstract.broadcast.authentication
        .AbstractAuthBroadcastMessage;

/**
 * Register message wrapper to communicate with Android Account Manager
 */
public class AccountRegisterBroadcastMessage extends AbstractAuthBroadcastMessage {

    public AccountRegisterBroadcastMessage(@NonNull Bundle data) {
        super(data);
    }

    @Override
    public void destroy() {
    }
}
