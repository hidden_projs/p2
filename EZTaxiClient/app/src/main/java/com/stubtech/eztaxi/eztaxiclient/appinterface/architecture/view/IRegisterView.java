package com.stubtech.eztaxi.eztaxiclient.appinterface.architecture.view;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.stubtech.eztaxi.eztaxiclient.apputil.Destroyable;

/**
 * Contract for Register Activity
 */
public interface IRegisterView extends Destroyable, IButterKnifeSupportingView,
                                       IDaggerInjetionSupportingView {

    @NonNull
    String getUsername();
    @NonNull
    String getPassword();
    @NonNull
    String getEmail();
    boolean isFinishing();
    void showToast(@Nullable String message);
    void finish();
    void setResult(int result, Intent intent);
}
