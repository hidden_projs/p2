package com.stubtech.eztaxi.eztaxiclient.appinterface.broadcast.auth;

import android.os.Bundle;

import com.stubtech.eztaxi.eztaxiclient.apputil.Destroyable;

/**
 * Interface to broadcast objects around framework.
 * Specific to be used with Android Auth Manager
 * Substitutes Intent
 */
public interface IAuthBroadcastMessage extends Destroyable {

    Bundle getData();
}
