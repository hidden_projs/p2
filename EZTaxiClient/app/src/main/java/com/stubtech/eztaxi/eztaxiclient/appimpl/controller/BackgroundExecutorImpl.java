package com.stubtech.eztaxi.eztaxiclient.appimpl.controller;

import com.google.common.collect.Lists;
import com.stubtech.eztaxi.eztaxiclient.appinterface.controller.IBackgroundExecutor;
import com.stubtech.eztaxi.eztaxiclient.apputil.Destroyable;

import java.util.Collection;

import rx.Subscription;
import rx.functions.Action0;
import rx.schedulers.Schedulers;

/**
 * Implementation class for communication in background
 *
 * @see com.stubtech.eztaxi.eztaxiclient.appinterface.controller.IBackgroundExecutor
 */
public class BackgroundExecutorImpl implements IBackgroundExecutor, Destroyable {

    private final Collection<Subscription> subscriptions = Lists.newLinkedList();

    @Override
    public synchronized void execute(Action0 action) {
        final Subscription subscription = Schedulers.newThread().createWorker().schedule(action);
        subscriptions.add(subscription);
    }

    @Override
    public synchronized void destroy() {
        for (Subscription subscription : subscriptions) {
            subscription.unsubscribe();
        }
        subscriptions.clear();
    }
}
