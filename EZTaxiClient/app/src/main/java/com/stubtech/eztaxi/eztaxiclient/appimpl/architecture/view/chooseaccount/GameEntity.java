package com.stubtech.eztaxi.eztaxiclient.appimpl.architecture.view.chooseaccount;

/**
 * Created by Batousik on 01.03.2016.
 */
public class GameEntity {

    public int imageResId;
    public int titleResId;

    public GameEntity(int imageResId, int titleResId) {
        this.imageResId = imageResId;
        this.titleResId = titleResId;
    }
}
