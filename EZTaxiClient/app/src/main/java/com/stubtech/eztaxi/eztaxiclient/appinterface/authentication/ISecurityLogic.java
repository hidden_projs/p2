package com.stubtech.eztaxi.eztaxiclient.appinterface.authentication;

import com.stubtech.eztaxi.eztaxiclient.apputil.Destroyable;

/**
 * Restricts unauthorised access, fetches token on expiration,
 * performs logic based on currently saved accounts
 */
public interface ISecurityLogic extends Destroyable {

    boolean doSecurityLogic();
}
