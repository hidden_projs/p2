package com.stubtech.eztaxi.eztaxiclient.old;

import android.app.Activity;
import android.os.Bundle;

import com.stubtech.eztaxi.eztaxiclient.R;

public class BlankActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_blank);
    }
}
