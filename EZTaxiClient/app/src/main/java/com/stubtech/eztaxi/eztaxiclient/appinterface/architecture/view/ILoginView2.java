package com.stubtech.eztaxi.eztaxiclient.appinterface.architecture.view;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;

/**
 * Contract for Login Activity
 */
public interface ILoginView2 {

    @NonNull
    String getUsername();
    @NonNull
    String getPassword();
    @NonNull
    String getAuthTokenType();
    void onErrorOccured(@NonNull String message);
    void onUserSignedIn(@NonNull Bundle result);
    void setResult(int result, Intent data);
    boolean isNewAccountRequested();
}
