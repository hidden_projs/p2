package com.stubtech.eztaxi.eztaxiclient.appimpl.architecture.model;

/**
 * POJO that is used to parse response from remote auth service
 */
public class ResponseParser {

    public final String createdAt;
    public final String objectId;
    public final String sessionToken;
    public final String updatedAt;
    public final String username;

    public ResponseParser(String createdAt,
                          String objectId,
                          String sessionToken,
                          String updatedAt,
                          String username) {
        this.createdAt = createdAt;
        this.objectId = objectId;
        this.sessionToken = sessionToken;
        this.updatedAt = updatedAt;
        this.username = username;
    }
}
