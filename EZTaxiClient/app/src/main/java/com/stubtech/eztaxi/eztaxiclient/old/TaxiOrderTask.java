package com.stubtech.eztaxi.eztaxiclient.old;

import android.app.Activity;
import android.os.AsyncTask;
import android.util.Log;

import com.stubtech.eztaxi.commonmodel.model.TaxiOrder;
import com.stubtech.eztaxi.eztaxiclient.R;
import com.stubtech.eztaxi.eztaxiclient.apputil.Constants;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;

//import com.fasterxml.jackson.core.JsonProcessingException;
//import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * Created by Batousik on 26.02.2016.
 */
public class TaxiOrderTask extends AsyncTask<TaxiOrder, Void, ServerReply> {
    private Activity activity;

    public TaxiOrderTask(Activity activity) {
        this.activity = activity;
    }

    @Override
    protected ServerReply doInBackground(TaxiOrder... form) {

        try {
            InputStream is;
//            ObjectMapper mapper = new ObjectMapper();
            String request = null;
            HttpURLConnection conn;

//            request = mapper.writeValueAsString(form);
            conn = ConnectionFactory.createConnection(activity.getResources()
                    .getString(R.string.home_taxi_order_endpoint), AppAuthentificator
                    .getXAuthToken(activity.getApplicationContext()), request);
            // Starts the query
            conn.connect();

            // Get response
            int response = conn.getResponseCode();
            String a = conn.getResponseMessage();
            Log.d(Constants.DEBUG_TAG, "The response is: " + response + " " + a);

            if (response == HttpURLConnection.HTTP_OK) {
                is = conn.getInputStream();

                BufferedReader reader = new BufferedReader(new InputStreamReader(is, "UTF-8"), 8);
                StringBuilder sb = new StringBuilder();

                String line;
                while ((line = reader.readLine()) != null) {
                    sb.append(line + "\n");
                }
                String result = sb.toString();
                return new ServerReply(ServerReplyStatus.SUCCESS, result);
            } else {
                // inform user of incorrect login details
                Log.i(Constants.DEBUG_TAG, "Unsuccessful request");
                return new ServerReply(ServerReplyStatus.SUCCESS, "Make msg");
            }
        } catch (Exception e) {
            e.printStackTrace();
            return new ServerReply(ServerReplyStatus.SUCCESS, e.getMessage());
        }
    }
}
