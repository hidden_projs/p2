package com.stubtech.eztaxi.eztaxiclient.appimpl.authentication;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.accounts.AccountManagerFuture;
import android.app.Activity;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.widget.ArrayAdapter;

import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableList;
import com.stubtech.eztaxi.eztaxiclient.R;
import com.stubtech.eztaxi.eztaxiclient.appinterface.architecture.view.IAccountHandlerView;
import com.stubtech.eztaxi.eztaxiclient.appinterface.authentication.IAccountManagerHelper;
import com.stubtech.eztaxi.eztaxiclient.appinterface.controller.IBackgroundExecutor;
import com.stubtech.eztaxi.eztaxiclient.apputil.Constants;

import java.util.Collections;
import java.util.List;

import javax.inject.Inject;

public class AccountManagerHelperImpl implements IAccountManagerHelper {

    @Inject
    IBackgroundExecutor executor;

    @Inject
    AccountManager accountManager;

    @Override
    public void addNewAccount(String access, IAccountHandlerView view, Activity activity) {
        Preconditions.checkNotNull(access);
        accountManager.addAccount(Constants.ACC_TYPE, access, null, null, activity, future1 -> {
            try {
                future1.getResult();
                view.onAccountAdded();
            } catch (Exception e) {
                view.onErrorOccurred(e.getMessage());
            }
        }, null);
    }

    @Override
    public void getAuthenticationTokenByFeatures(String access,
                                                 IAccountHandlerView view,
                                                 Activity activity) {
        Preconditions.checkNotNull(access);
        accountManager.getAuthTokenByFeatures(Constants.ACC_TYPE, access, null, activity, null,
                null, future1 -> {
                    try {
                        Bundle data = future1.getResult();
                        final String authToken = data.getString(AccountManager.KEY_AUTHTOKEN);
                        view.onRequestedTokenObtained(authToken);
                    } catch (Exception e) {
                        e.printStackTrace();
                        view.onErrorOccurred(e.getMessage());
                    }
                }, null);
    }

    @Override
    public List<String> getApplicationAccountNames() {
        Account[] availableAccounts = getAccountsByType();
        final int size = availableAccounts.length;
        if (size == 0) {
            return Collections.emptyList();
        }
        final ImmutableList.Builder<String> builder = ImmutableList.builder();
        for (final Account account : availableAccounts) {
            builder.add(account.name);
        }
        return builder.build();
    }

    @Override
    public Account getRegisteredAccount(int index) {
        return getAccountsByType()[index];
    }

    @Override
    public void removeAccount(int which, IAccountHandlerView view) {
        final Account account = getAccountsByType()[which];
        accountManager.removeAccount(account, future -> {
            try {
                future.getResult();
                view.onAccountRemoved(account);
            } catch (Exception e) {
                e.printStackTrace();
                view.onErrorOccurred(e.getMessage());
            }
        }, null);
    }

    @Override
    public void loadExistingAccountAuthToken(int which,
                                             String access,
                                             Activity activity,
                                             IAccountHandlerView view) {
        final Account account = getRegisteredAccount(which);
        final AccountManagerFuture<Bundle> future = accountManager.getAuthToken(account, access,
                null, activity, null, null);
        executor.execute(() -> {
            try {
                Bundle bnd = future.getResult();
                final String authtoken = bnd.getString(AccountManager.KEY_AUTHTOKEN);
                if (!TextUtils.isEmpty(authtoken)) {
                    view.onAuthTokenLoaded(authtoken);
                } else {
                    view.onErrorOccurred(activity.getString(R.string.error));
                }
            } catch (Exception e) {
                e.printStackTrace();
                view.onErrorOccurred(e.getMessage());
            }
        });
    }

    @Override
    public void invalidateAuthToken(int which,
                                    String access,
                                    Activity activity,
                                    IAccountHandlerView view) {
        final Account account = getRegisteredAccount(which);
        final AccountManagerFuture<Bundle> future = accountManager.getAuthToken(account, access,
                null, activity, null, null);
        executor.execute(() -> {
            try {
                Bundle result = future.getResult();
                final String authtoken = result.getString(AccountManager.KEY_AUTHTOKEN);
                accountManager.invalidateAuthToken(account.type, authtoken);
                view.onAuthTokenInvalidated(account);
            } catch (Exception e) {
                e.printStackTrace();
                view.onErrorOccurred(e.getMessage());
            }
        });
    }

    @Override
    public void showAccountsPicker(DialogInterface.OnClickListener clickListener,
                                   IAccountHandlerView view,
                                   Activity activity) {
        Preconditions.checkNotNull(clickListener);
        final List<String> accountsNames = getApplicationAccountNames();
        if (accountsNames.isEmpty()) {
            view.onNoAccountsCreated();
        } else {
            final ArrayAdapter adapter = new ArrayAdapter<>(activity, android.R.layout
                    .simple_list_item_1, accountsNames);
            new AlertDialog.Builder(activity).setTitle(activity.getString(R.string.pick_account))
                    .setAdapter(adapter, clickListener)
                    .create()
                    .show();
        }
    }

    public Account[] getAccountsByType() {
        return accountManager.getAccountsByType(Constants.ACC_TYPE);
    }

    @Override
    public void destroy() {
    }
}
