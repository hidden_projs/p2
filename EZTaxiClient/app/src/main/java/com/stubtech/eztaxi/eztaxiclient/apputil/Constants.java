package com.stubtech.eztaxi.eztaxiclient.apputil;

/**
 * Constants class for various purposes
 */
public abstract class Constants {

    public static final String DEBUG_TAG = "EZTAXI_DEBUG_TAG";
    public static final String LOGIN_STATE = "logged_in";
    public static final String X_AUTH_TOKEN = "X-AUTH-TOKEN";
    public static final String ACC_TYPE = "com.stubtech.eztaxi";
    public static final String ACC_NAME = "EZTaxi Account";
    public static final String IS_NEW_ACC = "is_new_acc";

    public static final class Parse {

        public static final String PARAMETER_USERNAME = "un";
        public static final String PARAMETER_PASSWORD = "pw";
        public static final String PARAMETER_EMAIL = "email";

        private Parse() {
        }
    }

    public static final class Access {

        public static final String AUTH_TOKEN_TYPE_READ_ONLY = "Read Only";
        public static final String AUTH_TOKEN_TYPE_READ_ONLY_LABEL = "Read only access to DD " +
                "account";
        public static final String AUTH_TOKEN_FULL_ACCESS = "Full Access";
        public static final String AUTH_TOKEN_FULL_ACCESS_LABEL = "Full access to DD account";

        private Access() {
        }
    }

    public static final class AccountCarousel {

        public final static int PAGES = 5;
        // You can choose a bigger number for LOOPS, but you know, nobody will fling
        // more than 1000 times just in order to test your "infinite" ViewPager :D
        public final static int LOOPS = 1000;
        public final static int FIRST_PAGE = PAGES * LOOPS / 2;
        public final static float BIG_SCALE = 1.0f;
        public final static float SMALL_SCALE = 0.7f;
        public final static float DIFF_SCALE = BIG_SCALE - SMALL_SCALE;
    }

    public static final class SecurityConstants {
        public static final String UN = "UN";
        public static final String PW = "PW";
        public static final String TOKEN = "X-AUTH-TOKEN";
        public static final String LOGIN_STATE = "LOGIN_STATE";
    }
}
