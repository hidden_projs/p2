package com.stubtech.eztaxi.eztaxiclient.appimpl.architecture.controller;

import com.stubtech.eztaxi.eztaxiclient.appanotation.ui.ActivityScope;
import com.stubtech.eztaxi.eztaxiclient.appinterface.architecture.controller.IRegisterController;

/**
 * Implementation of injectable Controller for the Register Activity
 */
@ActivityScope
public class RegisterControllerImpl implements IRegisterController {

    @Override
    public void register() {
    }

    @Override
    public void destroy() {
    }
}
