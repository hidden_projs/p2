package com.stubtech.eztaxi.eztaxiclient.appinterface.broadcast;

import android.support.annotation.NonNull;

import com.stubtech.eztaxi.eztaxiclient.apputil.Destroyable;

/**
 * Interface to broadcast objects around framework.
 * Substitutes Intent
 */
public interface IBroadcastManager extends Destroyable {

    void register(@NonNull Object object);
    void unregister(@NonNull Object object);
    void post(@NonNull Object object);
}
