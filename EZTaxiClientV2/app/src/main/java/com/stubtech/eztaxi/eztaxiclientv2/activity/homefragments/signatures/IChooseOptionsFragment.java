package com.stubtech.eztaxi.eztaxiclientv2.activity.homefragments.signatures;

import android.support.v7.widget.SwitchCompat;
import android.widget.Spinner;

/**
 * Created by Batousik on 07.03.2016.
 */
public interface IChooseOptionsFragment {

    Spinner getmCarTypeSpinner();
    Spinner getmPaymentTypeSpinner();
    SwitchCompat getmChildSeatSwitch();
    SwitchCompat getmPetTransportedSwitch();
}
