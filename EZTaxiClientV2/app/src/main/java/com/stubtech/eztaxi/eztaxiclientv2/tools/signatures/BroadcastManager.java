package com.stubtech.eztaxi.eztaxiclientv2.tools.signatures;

import android.support.annotation.NonNull;

/**
 * Interface to broadcast objects around framework.
 * Substitutes Intent
 */
public interface BroadcastManager {

    void register(@NonNull Object receiver);
    void unregister(@NonNull Object receiver);
    void post(@NonNull Object event);
}
