package com.stubtech.eztaxi.eztaxiclientv2.di.signatures;

public interface Destroyable {

    void destroy();
}
