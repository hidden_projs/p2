package com.stubtech.eztaxi.eztaxiclientv2.di.module;

import android.content.Context;

import com.squareup.okhttp.OkHttpClient;
import com.stubtech.eztaxi.eztaxiclientv2.security.AuthenticationProviderImpl;
import com.stubtech.eztaxi.eztaxiclientv2.security.signatures.AuthenticationProvider;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * This module provides:
 * <p/>
 * Custom Authenticator Service
 */
@Module
public class AuthenticationModule {

    @Provides
    @Singleton
    public AuthenticationProvider provideAuthenticationManager(OkHttpClient okHttpClient,
                                                               Context context) {
        return new AuthenticationProviderImpl(okHttpClient, context);
    }
}
