package com.stubtech.eztaxi.eztaxiclientv2.di.module;

import android.support.v4.app.Fragment;

import com.stubtech.eztaxi.eztaxiclientv2.activity.homefragments.signatures.IChooseOptionsFragment;
import com.stubtech.eztaxi.eztaxiclientv2.di.signatures.ActivityScope;

import dagger.Module;
import dagger.Provides;

@Module
public class FragmentModule {

    private final Fragment fragment;

    public FragmentModule(Fragment fragment) {
        this.fragment = fragment;
    }

    @Provides
    @ActivityScope
    Fragment provideActivity() {
        return fragment;
    }

    @Provides
    @ActivityScope
    IChooseOptionsFragment provideIChooseOprionsFragment() {
        return (IChooseOptionsFragment) fragment;
    }
}
