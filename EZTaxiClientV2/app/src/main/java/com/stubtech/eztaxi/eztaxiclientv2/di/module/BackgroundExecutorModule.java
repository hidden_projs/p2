package com.stubtech.eztaxi.eztaxiclientv2.di.module;

import com.stubtech.eztaxi.eztaxiclientv2.tools.BackgroundExecutorImpl;
import com.stubtech.eztaxi.eztaxiclientv2.tools.signatures.BackgroundExecutor;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * This module provides:
 * <p/>
 * Enhancement for android AsyncTask.
 * <p/>
 * ReactiveX is a combination of the best ideas from the Observer pattern, the Iterator pattern,
 * and functional programming
 * <p/>
 * Easily create event streams or data streams.
 * Compose and transform streams with query-like operators.
 * Subscribe to any observable stream to perform side effects.
 */
@Module
public class BackgroundExecutorModule {

    @Provides
    @Singleton
    BackgroundExecutor provideBackgroundExecutor() {
        return new BackgroundExecutorImpl();
    }
}
