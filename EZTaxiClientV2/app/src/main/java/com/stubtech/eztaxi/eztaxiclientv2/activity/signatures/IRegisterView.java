package com.stubtech.eztaxi.eztaxiclientv2.activity.signatures;

import android.support.design.widget.TextInputLayout;
import android.support.v7.widget.AppCompatButton;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

public interface IRegisterView extends IDaggerInjected, IButteredKnifeActivity {

    AppCompatButton getmBtnRegister();
    EditText getmTxtUN();
    EditText getmTxtPWFirst();
    EditText getmTxtPWSecond();
    EditText getmTxtNumber();
    TextInputLayout getmWrapperUN();
    TextInputLayout getmWrapperPWFirst();
    TextInputLayout getmWrapperPWSecond();
    TextInputLayout getmWrapperNumber();
    Spinner getmSpinnerCountryCode();
    TextView getmTxtCountryCode();
}
