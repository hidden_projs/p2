package com.stubtech.eztaxi.eztaxiclientv2.activity.controller.signatures;

import com.google.common.eventbus.Subscribe;
import com.stubtech.eztaxi.eztaxiclientv2.di.signatures.Destroyable;
import com.stubtech.eztaxi.eztaxiclientv2.pojo.ApiResponse;

public interface IRegisterActivityController extends Destroyable {

    void attemptRegister();
    @Subscribe
    void onEvent(ApiResponse event);
    void confirmRegistration(String token);
    void showRegistrationError(String error);
    void goToLogin();
    boolean validateUserInput();
}
