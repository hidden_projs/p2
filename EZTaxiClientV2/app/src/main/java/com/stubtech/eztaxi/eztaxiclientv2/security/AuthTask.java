package com.stubtech.eztaxi.eztaxiclientv2.security;

import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.util.Log;

import com.google.common.base.Preconditions;
import com.stubtech.eztaxi.eztaxiclientv2.pojo.ApiResponse;
import com.stubtech.eztaxi.eztaxiclientv2.pojo.User;
import com.stubtech.eztaxi.eztaxiclientv2.security.signatures.AuthenticationProvider;
import com.stubtech.eztaxi.eztaxiclientv2.tools.signatures.BroadcastManager;
import com.stubtech.eztaxi.eztaxiclientv2.util.Utils;

public class AuthTask extends AsyncTask<User, Void, ApiResponse> {

    private final AuthenticationProvider authenticationProvider;
    private final BroadcastManager broadcastManager;

    public AuthTask(@NonNull AuthenticationProvider authenticationProvider,
                    @NonNull BroadcastManager broadcastManager) {
        Preconditions.checkNotNull(broadcastManager);
        Preconditions.checkNotNull(authenticationProvider);
        this.authenticationProvider = authenticationProvider;
        this.broadcastManager = broadcastManager;
    }

    @Override
    protected ApiResponse doInBackground(User... params) {
        Log.d(Utils.Constants.DEBUG_TAG, "Started Login task");
        ApiResponse apiResponse;
        try {
            apiResponse = authenticationProvider.login(params[0]);
            return apiResponse;
        } catch (Exception e) {
            return new ApiResponse(Utils.Constants.APIRESPONSECODES.ERROR, e.getMessage());
        }
    }

    @Override
    protected void onPostExecute(ApiResponse apiResponse) {
        broadcastManager.post(apiResponse);
    }
}
