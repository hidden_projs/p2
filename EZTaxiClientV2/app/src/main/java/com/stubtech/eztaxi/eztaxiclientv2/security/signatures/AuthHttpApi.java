package com.stubtech.eztaxi.eztaxiclientv2.security.signatures;

import com.squareup.okhttp.RequestBody;
import com.stubtech.eztaxi.eztaxiclientv2.pojo.ApiResponse;

import retrofit.Call;
import retrofit.http.Body;
import retrofit.http.POST;

/**
 * Interface to be used with retrofit for simple rest api calls
 * Specific for External service authentication
 */
public interface AuthHttpApi {

    @POST("/eztaxiserver/auth/login")
    Call<ApiResponse> login(@Body RequestBody requestBody);

    @POST("/eztaxiserver/auth/register")
    Call<ApiResponse> register(@Body RequestBody requestBody);
}
