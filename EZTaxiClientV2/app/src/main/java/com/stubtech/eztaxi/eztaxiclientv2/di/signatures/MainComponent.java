package com.stubtech.eztaxi.eztaxiclientv2.di.signatures;

/**
 * This interface provides signatures to Dagger to add all modules to the Application scoped
 * DependencyGraph
 */
public interface MainComponent extends DependencyGraph {

    final class Initializer {

        /**
         * Forbid Object construction.
         */
        private Initializer() {
        }

        /**
         * Explicitly build graph since modules have constructor parameter.
         *
         * @param moduleSupplier
         * @return
         */

        public static DependencyGraph initialize(ModuleSupplier moduleSupplier) {
            return DaggerDependencyGraph.builder()
                    .applicationModule(moduleSupplier.provideApplicationModule())
                    .authenticationModule(moduleSupplier.provideAuthenticationModule())
                    .broadcastModule(moduleSupplier.provideBroadcastModule())
                    .httpModule(moduleSupplier.provideHttpModule())
                    .memoryLeakDetectionModule(moduleSupplier.provideMemoryLeakDetectionModule())
                    .build();
        }
    }
}
