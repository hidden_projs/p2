package com.stubtech.eztaxi.eztaxiclientv2.pojo;

public class ApiResponse {

    private int code;
    private String payload;

    public ApiResponse(int code, String payload) {
        this.code = code;
        this.payload = payload;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getPayload() {
        return payload;
    }

    public void setPayload(String payload) {
        this.payload = payload;
    }
}
