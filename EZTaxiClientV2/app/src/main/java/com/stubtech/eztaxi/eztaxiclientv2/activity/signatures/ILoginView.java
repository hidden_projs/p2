package com.stubtech.eztaxi.eztaxiclientv2.activity.signatures;

import android.support.design.widget.TextInputLayout;
import android.support.v7.widget.AppCompatButton;
import android.widget.TextView;

public interface ILoginView extends IDaggerInjected, IButteredKnifeActivity {

    AppCompatButton getmBtnLogin();
    TextInputLayout getmWrapperUN();
    TextInputLayout getmWrapperPW();
    TextView getmTxtUN();
    TextView getmTxtPW();
}
