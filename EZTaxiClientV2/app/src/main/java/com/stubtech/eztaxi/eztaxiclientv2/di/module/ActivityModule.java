package com.stubtech.eztaxi.eztaxiclientv2.di.module;

import android.app.Activity;
import android.os.Bundle;

import com.stubtech.eztaxi.eztaxiclientv2.activity.signatures.IHomeView;
import com.stubtech.eztaxi.eztaxiclientv2.activity.signatures.ILoginView;
import com.stubtech.eztaxi.eztaxiclientv2.activity.signatures.IRegisterView;
import com.stubtech.eztaxi.eztaxiclientv2.di.signatures.ActivityScope;

import dagger.Module;
import dagger.Provides;

/**
 * This module injects Activities to Dagger
 */
@Module
public class ActivityModule {

    private final Activity activity;

    public ActivityModule(Activity activity) {
        this.activity = activity;
    }

    @Provides
    @ActivityScope
    Activity provideActivity() {
        return activity;
    }

    @Provides
    @ActivityScope
    Bundle provideExtras() {
        return activity.getIntent().getExtras();
    }

    @Provides
    @ActivityScope
    IHomeView provideIHomeView() {
        return (IHomeView) activity;
    }

    @Provides
    @ActivityScope
    ILoginView provideLoginActivity() {
        return (ILoginView) activity;
    }

    @Provides
    @ActivityScope
    IRegisterView provideRegisterActivity() {
        return (IRegisterView) activity;
    }
}
