package com.stubtech.eztaxi.eztaxiclientv2.di;

import android.app.Activity;
import android.support.annotation.NonNull;

import com.google.common.base.Preconditions;
import com.squareup.leakcanary.RefWatcher;
import com.stubtech.eztaxi.eztaxiclientv2.di.module.ActivityModule;
import com.stubtech.eztaxi.eztaxiclientv2.di.signatures.ActivityComponent;
import com.stubtech.eztaxi.eztaxiclientv2.di.signatures.DaggerActivityComponent;
import com.stubtech.eztaxi.eztaxiclientv2.di.signatures.DependencyGraph;
import com.stubtech.eztaxi.eztaxiclientv2.di.signatures.MainComponent;
import com.stubtech.eztaxi.eztaxiclientv2.di.signatures.ModuleSupplier;

/**
 * Dagger key class
 */
public class DependencyInjector {

    private static final String PRECONDITION_MESSAGE_GRAPH_NOT_INITIALIZED = "Dependency graph "
            + "not initialized";
    private static DependencyGraph mainComponent;

    /**
     * Forbid Object construction.
     */
    private DependencyInjector() {
    }

    public static synchronized void initialize(ModuleSupplier supplier) {
        mainComponent = MainComponent.Initializer.initialize(supplier);
    }

    public static synchronized boolean isInitialized() {
        return mainComponent != null;
    }

    public static synchronized DependencyGraph getGraph() {
        Preconditions.checkState(isInitialized(), PRECONDITION_MESSAGE_GRAPH_NOT_INITIALIZED);
        return mainComponent;
    }

    /**
     * Method creates scoped graph of injectable dependencies per activity
     * Whenever new activity starts, Activity module is replaced for that activity
     */
    public static ActivityComponent activityComponent(@NonNull final Activity activity) {
        return DaggerActivityComponent.builder()
                .dependencyGraph(getGraph())
                .activityModule(new ActivityModule(activity))
                .build();
    }

    /**
     * Inject RefWatcher to kill objects that leak memory
     *
     * @return
     */
    public static RefWatcher refWatcher() {
        return getGraph().refWatcher().get();
    }
}
