package com.stubtech.eztaxi.eztaxiclientv2.security;

import android.content.SharedPreferences;

import com.stubtech.eztaxi.eztaxiclientv2.EZTaxiClientApplication;
import com.stubtech.eztaxi.eztaxiclientv2.util.Utils;

public class AppSecurityManager {

    public static void logIn(String token) {
        SharedPreferences.Editor editor = EZTaxiClientApplication.preferences.edit();
        editor.putBoolean(Utils.Constants.LOGIN_STATE, true);
        editor.putString(Utils.Constants.X_AUTH_TOKEN, token);
        editor.commit();
    }

    public static void logOut() {
        SharedPreferences.Editor editor = EZTaxiClientApplication.preferences.edit();
        editor.putBoolean(Utils.Constants.LOGIN_STATE, false);
        editor.putString(Utils.Constants.X_AUTH_TOKEN, null);
        editor.commit();
    }

    public boolean isLoggedIn() {
        return EZTaxiClientApplication.preferences.getBoolean(Utils.Constants.LOGIN_STATE, false);
    }

    public String getToken() {
        return EZTaxiClientApplication.preferences.getString(Utils.Constants.X_AUTH_TOKEN, "");
    }
}
