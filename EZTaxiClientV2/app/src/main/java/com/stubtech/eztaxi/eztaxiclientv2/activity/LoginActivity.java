package com.stubtech.eztaxi.eztaxiclientv2.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.stubtech.eztaxi.eztaxiclientv2.R;
import com.stubtech.eztaxi.eztaxiclientv2.activity.controller.LoginActivityController;
import com.stubtech.eztaxi.eztaxiclientv2.activity.signatures.ILoginView;
import com.stubtech.eztaxi.eztaxiclientv2.di.DependencyInjector;
import com.stubtech.eztaxi.eztaxiclientv2.di.signatures.ActivityScope;
import com.stubtech.eztaxi.eztaxiclientv2.util.Utils;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class LoginActivity extends AppCompatActivity implements ILoginView {

    @Inject
    @ActivityScope
    LoginActivityController loginActivityController;

    @Bind(R.id.login_btn_login)
    AppCompatButton mBtnLogin;

    @Bind(R.id.login_wrapper_un)
    TextInputLayout mWrapperUN;
    @Bind(R.id.login_wrapper_pw)
    TextInputLayout mWrapperPW;

    @Bind(R.id.login_txt_un)
    TextView mTxtUN;
    @Bind(R.id.login_txt_pw)
    TextView mTxtPW;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        bindViews();
        injectMembers();
        mWrapperUN.setErrorEnabled(true);
        mWrapperPW.setErrorEnabled(true);
        mTxtUN.setText("test@test.com");
        mTxtPW.setText("test");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        loginActivityController.destroy();
    }

    @OnClick(R.id.login_btn_login)
    void onBtnLoginClick(View view) {
        Log.d(Utils.Constants.DEBUG_TAG, "Login Button Clicked");
        loginActivityController.attemptLogin();
    }

    @OnClick(R.id.login_link_register)
    void onLinkRegisterClick(View view) {
        loginActivityController.goToRegister();
    }

    public void bindViews() {
        ButterKnife.bind(this);
    }

    public void injectMembers() {
        DependencyInjector.activityComponent(this).inject(this);
    }

    public AppCompatButton getmBtnLogin() {
        return mBtnLogin;
    }

    public TextInputLayout getmWrapperUN() {
        return mWrapperUN;
    }

    public TextInputLayout getmWrapperPW() {
        return mWrapperPW;
    }

    public TextView getmTxtUN() {
        return mTxtUN;
    }

    public TextView getmTxtPW() {
        return mTxtPW;
    }

    @OnClick(R.id.login_btn_skip_login)
    void debugSkipAuth() {
        Intent intent = new Intent(this, HomeActivity.class);
        this.startActivity(intent);
        this.finishActivity(0);
    }
}
