package com.stubtech.eztaxi.eztaxiclientv2.activity;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.stubtech.eztaxi.eztaxiclientv2.R;
import com.stubtech.eztaxi.eztaxiclientv2.activity.controller.HomeActivityController;
import com.stubtech.eztaxi.eztaxiclientv2.activity.homefragments.ChooseOptionsFragment;
import com.stubtech.eztaxi.eztaxiclientv2.activity.homefragments.HomeViewPagerAdapter;
import com.stubtech.eztaxi.eztaxiclientv2.activity.homefragments.OrderTaxiFragment;
import com.stubtech.eztaxi.eztaxiclientv2.activity.homefragments.PickLocationsFragment;
import com.stubtech.eztaxi.eztaxiclientv2.activity.homefragments.signatures.IChooseOptionsFragment;
import com.stubtech.eztaxi.eztaxiclientv2.activity.signatures.IHomeView;
import com.stubtech.eztaxi.eztaxiclientv2.di.DependencyInjector;
import com.stubtech.eztaxi.eztaxiclientv2.di.signatures.ActivityScope;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;

public class HomeActivity extends AppCompatActivity implements IHomeView {
    //    @Bind(R.id.home_btn_logout)
    //    Button mBtnLogout;
    //    @Bind(R.id.home_btn_order_taxi)
    //    Button mBtnOrderTaxi;

    public static FragmentManager fragmentManager;

    @Bind(R.id.home_drawer_layout)
    DrawerLayout drawerLayout;

    @Bind(R.id.home_toolbar)
    Toolbar toolbar;

    @Bind(R.id.home_tab_layout)
    TabLayout tabLayout;

    @Bind(R.id.home_viewpager)
    ViewPager viewPager;

    Fragment orderTaxiFragment;
    Fragment pickLocationsFragment;
    IChooseOptionsFragment chooseOptionsFragment;

    @Inject
    @ActivityScope
    HomeActivityController homeActivityController;

    public void bindViews() {
        ButterKnife.bind(this);
    }

    public void injectMembers() {
        DependencyInjector.activityComponent(this).inject(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        fragmentManager = getSupportFragmentManager();
        injectMembers();
        bindViews();
        setupToolbar();
        setupViewPager();
        setupTabLayout();
        // homeActivityController.setUp();
    }

    private void setupViewPager() {
        HomeViewPagerAdapter adapter = new HomeViewPagerAdapter(getSupportFragmentManager());
        orderTaxiFragment = new OrderTaxiFragment();
        chooseOptionsFragment = new ChooseOptionsFragment();
        pickLocationsFragment = new PickLocationsFragment();
        adapter.addFragment(orderTaxiFragment, "Order");
        adapter.addFragment(pickLocationsFragment, "Locations");
        adapter.addFragment((Fragment) chooseOptionsFragment, "Options");
        viewPager.setAdapter(adapter);
    }

    private void setupTabLayout() {
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
        tabLayout.setupWithViewPager(viewPager);
    }

    private void setupToolbar() {
        setSupportActionBar(toolbar);
        // Show menu icon
        final ActionBar ab = getSupportActionBar();
        ab.setHomeAsUpIndicator(R.mipmap.logo3);
        ab.setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                drawerLayout.openDrawer(GravityCompat.START);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public Fragment getOrderTaxiFragment() {
        return orderTaxiFragment;
    }

    @Override
    public Fragment getPickLocationsFragment() {
        return pickLocationsFragment;
    }

    @Override
    public IChooseOptionsFragment getChooseOptionsFragment() {
        return chooseOptionsFragment;
    }

    @Override
    protected void onDestroy() {
        homeActivityController.destroy();
        super.onDestroy();

    }
}
