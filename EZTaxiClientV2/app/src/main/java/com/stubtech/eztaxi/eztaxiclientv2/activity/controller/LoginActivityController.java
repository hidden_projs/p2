package com.stubtech.eztaxi.eztaxiclientv2.activity.controller;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;

import com.google.common.base.Preconditions;
import com.google.common.eventbus.Subscribe;
import com.stubtech.eztaxi.eztaxiclientv2.activity.HomeActivity;
import com.stubtech.eztaxi.eztaxiclientv2.activity.RegisterActivity;
import com.stubtech.eztaxi.eztaxiclientv2.activity.controller.signatures.ILoginActivityController;
import com.stubtech.eztaxi.eztaxiclientv2.activity.signatures.ILoginView;
import com.stubtech.eztaxi.eztaxiclientv2.di.signatures.ActivityScope;
import com.stubtech.eztaxi.eztaxiclientv2.pojo.ApiResponse;
import com.stubtech.eztaxi.eztaxiclientv2.pojo.User;
import com.stubtech.eztaxi.eztaxiclientv2.security.AppSecurityManager;
import com.stubtech.eztaxi.eztaxiclientv2.security.AuthTask;
import com.stubtech.eztaxi.eztaxiclientv2.security.signatures.AuthenticationProvider;
import com.stubtech.eztaxi.eztaxiclientv2.tools.signatures.BroadcastManager;
import com.stubtech.eztaxi.eztaxiclientv2.util.Utils;

import javax.inject.Inject;

@ActivityScope
public class LoginActivityController implements ILoginActivityController {

    private final Activity activity;
    private final ILoginView view;
    private final BroadcastManager broadcastManager;
    private final Context context;
    private final AuthenticationProvider authenticationProvider;

    @Inject
    public LoginActivityController(@NonNull ILoginView view,
                                   @NonNull Activity activity,
                                   @NonNull BroadcastManager broadcastManager,
                                   @NonNull Context context,
                                   @NonNull AuthenticationProvider authenticationProvider) {
        Preconditions.checkNotNull(activity);
        Preconditions.checkNotNull(broadcastManager);
        Preconditions.checkNotNull(view);
        Preconditions.checkNotNull(context);
        Preconditions.checkNotNull(authenticationProvider);
        this.broadcastManager = broadcastManager;
        this.activity = activity;
        this.view = view;
        this.context = context;
        this.authenticationProvider = authenticationProvider;
    }

    @Override
    public void attemptLogin() {
        if (validateUserInput()) {
            User user = new User();
            user.setUn(view.getmTxtUN().getText().toString().trim());
            user.setPw(view.getmTxtPW().getText().toString().trim());
            view.getmBtnLogin().setEnabled(false);
            view.getmBtnLogin().setAlpha(.5f);
            broadcastManager.register(this);
            new AuthTask(authenticationProvider, broadcastManager).execute(user);
        }
    }

    @Override
    @Subscribe
    public void onEvent(ApiResponse event) {
        broadcastManager.unregister(this);
        view.getmBtnLogin().setEnabled(true);
        view.getmBtnLogin().setAlpha(1f);
        if (event.getCode() == Utils.Constants.APIRESPONSECODES.OK) {
            confirmAuthentication(event.getPayload());
        } else {
            showAuthenticationError(event.getPayload());
        }
    }

    @Override
    public void confirmAuthentication(String token) {
        AppSecurityManager.logIn(token);
        Intent intent = new Intent(context, HomeActivity.class);
        activity.startActivity(intent);
        activity.finishActivity(0);
    }

    @Override
    public void showAuthenticationError(String error) {
        Utils.ToastCreator.showToast(activity, error);
    }

    @Override
    public void goToRegister() {
        Intent intent = new Intent(context, RegisterActivity.class);
        activity.startActivity(intent);
        activity.finishActivity(0);
    }

    @Override
    public boolean validateUserInput() {
        boolean valid = true;
        String un = view.getmTxtUN().getText().toString().trim();
        String pw = view.getmTxtPW().getText().toString().trim();
        if (un.isEmpty() || !android.util.Patterns.EMAIL_ADDRESS.matcher(un).matches()) {
            view.getmWrapperUN().setError("Enter a valid email address");
            valid = false;
        } else {
            view.getmWrapperUN().setError(null);
        }
        if (pw.isEmpty() || pw.length() < 4 || pw.length() > 10) {
            view.getmWrapperPW().setError("Between 4 and 10 alphanumeric characters");
            valid = false;
        } else {
            view.getmWrapperPW().setError(null);
        }
        return valid;
    }

    @Override
    public void destroy() {
        broadcastManager.unregister(this);
    }
}

