package com.stubtech.eztaxi.eztaxiclientv2.activity;

import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.stubtech.eztaxi.eztaxiclientv2.R;
import com.stubtech.eztaxi.eztaxiclientv2.activity.controller.RegisterActivityController;
import com.stubtech.eztaxi.eztaxiclientv2.activity.signatures.IRegisterView;
import com.stubtech.eztaxi.eztaxiclientv2.di.DependencyInjector;
import com.stubtech.eztaxi.eztaxiclientv2.di.signatures.ActivityScope;
import com.stubtech.eztaxi.eztaxiclientv2.util.Utils;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class RegisterActivity extends AppCompatActivity implements IRegisterView {

    @Inject
    @ActivityScope
    RegisterActivityController registerActivityController;

    @Bind(R.id.register_btn_register)
    AppCompatButton mBtnRegister;

    @Bind(R.id.register_txt_un)
    EditText mTxtUN;
    @Bind(R.id.register_txt_pw_first)
    EditText mTxtPWFirst;
    @Bind(R.id.register_txt_pw_second)
    EditText mTxtPWSecond;
    @Bind(R.id.register_txt_number)
    EditText mTxtNumber;
    @Bind(R.id.register_spinner_country_code)
    Spinner mSpinnerCountryCode;
    @Bind(R.id.register_txt_country_code)
    TextView mTxtCountryCode;

    @Bind(R.id.register_wrapper_un)
    TextInputLayout mWrapperUN;
    @Bind(R.id.register_wrapper_pw_first)
    TextInputLayout mWrapperPWFirst;
    @Bind(R.id.register_wrapper_pw_second)
    TextInputLayout mWrapperPWSecond;
    @Bind(R.id.register_wrapper_number)
    TextInputLayout mWrapperNumber;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        injectMembers();
        bindViews();
        registerActivityController.instantiatePhoneNumberUtils();
    }

    @OnClick(R.id.register_btn_register)
    void onBtnRegisterClick(View view) {
        Log.d(Utils.Constants.DEBUG_TAG, "Register Button Clicked");
        registerActivityController.attemptRegister();
    }

    @OnClick(R.id.register_link_login)
    void onLinkLoginClick(View view) {
        registerActivityController.goToLogin();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        registerActivityController.destroy();
    }

    @Override
    public void bindViews() {
        ButterKnife.bind(this);
    }

    @Override
    public void injectMembers() {
        DependencyInjector.activityComponent(this).inject(this);
    }

    @Override
    public AppCompatButton getmBtnRegister() {
        return mBtnRegister;
    }

    @Override
    public EditText getmTxtUN() {
        return mTxtUN;
    }

    @Override
    public EditText getmTxtPWFirst() {
        return mTxtPWFirst;
    }

    @Override
    public EditText getmTxtPWSecond() {
        return mTxtPWSecond;
    }

    @Override
    public EditText getmTxtNumber() {
        return mTxtNumber;
    }

    @Override
    public TextInputLayout getmWrapperUN() {
        return mWrapperUN;
    }

    @Override
    public TextInputLayout getmWrapperPWFirst() {
        return mWrapperPWFirst;
    }

    @Override
    public TextInputLayout getmWrapperPWSecond() {
        return mWrapperPWSecond;
    }

    @Override
    public TextInputLayout getmWrapperNumber() {
        return mWrapperNumber;
    }

    @Override
    public Spinner getmSpinnerCountryCode() {
        return mSpinnerCountryCode;
    }

    @Override
    public TextView getmTxtCountryCode() {
        return mTxtCountryCode;
    }
}
