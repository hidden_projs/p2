package com.stubtech.eztaxi.eztaxiclientv2.activity.signatures;

import android.support.v4.app.Fragment;

import com.stubtech.eztaxi.eztaxiclientv2.activity.homefragments.signatures.IChooseOptionsFragment;

public interface IHomeView extends IDaggerInjected, IButteredKnifeActivity {

    Fragment getOrderTaxiFragment();
    Fragment getPickLocationsFragment();
    IChooseOptionsFragment getChooseOptionsFragment();
}
