package com.stubtech.eztaxi.eztaxiclientv2.di.signatures;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Scope;

/**
 * Annotation to declare that a component will be used during activity
 * Created at start and removed after activity no longer exists
 */
@Scope
@Retention(RetentionPolicy.RUNTIME)
public @interface ActivityScope {

}
