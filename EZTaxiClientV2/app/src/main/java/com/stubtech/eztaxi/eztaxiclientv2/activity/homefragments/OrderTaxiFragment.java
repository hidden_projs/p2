package com.stubtech.eztaxi.eztaxiclientv2.activity.homefragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.stubtech.eztaxi.eztaxiclientv2.R;

public class OrderTaxiFragment extends Fragment {

    public OrderTaxiFragment() {
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.activity_home_fragment_order_taxi, container,
                false);
        return rootView;
    }
}
