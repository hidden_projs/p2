package com.stubtech.eztaxi.eztaxiclientv2.util;

import android.app.Application;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.widget.Toast;

import com.facebook.stetho.Stetho;
import com.stubtech.eztaxi.eztaxiclientv2.BuildConfig;

public class Utils {

    private Utils() {
    }

    public static boolean isThisAMainThread() {
        return Looper.getMainLooper() == Looper.myLooper();
    }

    public static final class ToastCreator {

        public static void showToast(final @NonNull Context context,
                                     final @NonNull String message) {
            Toast.makeText(context, message, Toast.LENGTH_LONG).show();
        }
    }

    public static final class ConnectionChecker {

        public static final boolean hasConnection(Context context) {
            ConnectivityManager connMgr;
            NetworkInfo networkInfo;
            // Check connectivity
            connMgr = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            networkInfo = connMgr.getActiveNetworkInfo();
            // Return if no connection
            return !(networkInfo == null || !networkInfo.isConnected());
        }
    }

    public static class Constants {

        public static final String DEBUG_TAG = "EZ_TAXI_V2_DEBUG_TAG";
        public static final String LOGIN_STATE = "LOGIN_STATE";
        public static final String X_AUTH_TOKEN = "X-AUTH-TOKEN";

        public static final class HTTPCODES {

            public static final int SC_OK = 200;
            public static final int SC_CREATED = 201;
            public static final int SC_NOT_FOUND = 404;
            public static final int SC_UNAUTHORIZED = 403;
            public static final int SC_BAD_REQUEST = 400;
        }

        public static final class USERCONST {

            public static final String PARAMETER_USERNAME = "un";
            public static final String PARAMETER_PASSWORD = "pw";
            public static final String PARAMETER_EMAIL = "email";
        }

        public static final class APIRESPONSECODES {

            public static final int ERROR = -1;
            public static final int OK = 1;
            public static final int UNAUTHORISED = 2;
        }
    }

    public static final class FacebookDebugBridge {

        /**
         * If debugging allows nice debug tool to use via chrome browser
         * <p/>
         * From API: Stetho is a sophisticated debug bridge for Android applications. When enabled,
         * developers have access to the Chrome Developer Tools feature natively part of the Chrome
         * desktop browser. Developers can also choose to enable the optional dumpapp tool which
         * offers a powerful command-line interface to application internals.
         *
         * @param application EZTaxiClientApplication
         */
        public static void installStetho(@NonNull Application application) {
            if (BuildConfig.DEBUG) {
                Stetho.initialize(Stetho.newInitializerBuilder(application)
                        .enableDumpapp(Stetho.defaultDumperPluginsProvider(application))
                        .enableWebKitInspector(Stetho.defaultInspectorModulesProvider(application))
                        .build());
            }
        }
    }
}
