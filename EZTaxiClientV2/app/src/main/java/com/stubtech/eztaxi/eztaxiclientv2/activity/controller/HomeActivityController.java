package com.stubtech.eztaxi.eztaxiclientv2.activity.controller;

import android.content.Context;
import android.support.annotation.NonNull;

import com.google.common.base.Preconditions;
import com.stubtech.eztaxi.eztaxiclientv2.activity.controller.signatures.IHomeActivityController;
import com.stubtech.eztaxi.eztaxiclientv2.activity.signatures.IHomeView;
import com.stubtech.eztaxi.eztaxiclientv2.di.signatures.ActivityScope;
import com.stubtech.eztaxi.eztaxiclientv2.tools.signatures.BroadcastManager;

import javax.inject.Inject;

@ActivityScope
public class HomeActivityController implements IHomeActivityController {

    private final IHomeView activity;
    private final BroadcastManager broadcastManager;
    private final Context context;

    @Inject
    public HomeActivityController(@NonNull IHomeView activity,
                                  @NonNull Context context,
                                  @NonNull BroadcastManager broadcastManager) {
        Preconditions.checkNotNull(activity);
        Preconditions.checkNotNull(broadcastManager);
        Preconditions.checkNotNull(context);
        this.context = context;
        this.activity = activity;
        this.broadcastManager = broadcastManager;
    }

    public void setUp() {
        //TODO: reimplement using dagger
        //        setUpOrderFragment();
        //        setUpOptionsFragment();
        //        setUpLocationsFragment();
    }

    private void setUpLocationsFragment() {
        //TODO:implement
    }

    private void setUpOrderFragment() {
        //TODO:implement
    }

    private void setUpOptionsFragment() {
    }

    @Override
    public void destroy() {
        broadcastManager.unregister(this);
    }
}
