package com.stubtech.eztaxi.eztaxiclientv2.security.signatures;

import com.stubtech.eztaxi.eztaxiclientv2.activity.signatures.IDaggerInjected;
import com.stubtech.eztaxi.eztaxiclientv2.pojo.ApiResponse;
import com.stubtech.eztaxi.eztaxiclientv2.pojo.User;

public interface AuthenticationProvider extends IDaggerInjected {

    ApiResponse login(User user) throws Exception;
    ApiResponse register(User user) throws Exception;
}
