package com.stubtech.eztaxi.eztaxiclientv2.security;

import android.content.Context;
import android.support.annotation.NonNull;

import com.google.common.base.Preconditions;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.RequestBody;
import com.stubtech.eztaxi.eztaxiclientv2.di.DependencyInjector;
import com.stubtech.eztaxi.eztaxiclientv2.pojo.ApiResponse;
import com.stubtech.eztaxi.eztaxiclientv2.pojo.User;
import com.stubtech.eztaxi.eztaxiclientv2.security.signatures.AuthHttpApi;
import com.stubtech.eztaxi.eztaxiclientv2.security.signatures.AuthenticationProvider;
import com.stubtech.eztaxi.eztaxiclientv2.util.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import retrofit.Call;
import retrofit.MoshiConverterFactory;
import retrofit.Response;
import retrofit.Retrofit;

public class AuthenticationProviderImpl implements AuthenticationProvider {

    private static final String BASE_URL = "http://192.168.0.13:8080";
    private static final String MEDIA_TYPE_APPLICATION_JSON = "application/json";
    private final Context context;
    private AuthHttpApi authParserImpl;

    public AuthenticationProviderImpl(final @NonNull OkHttpClient okHttpClient,
                                      final @NonNull Context context) {
        this.context = context;
        authParserImpl = new Retrofit.Builder().baseUrl(BASE_URL)
                .addConverterFactory(MoshiConverterFactory.create())
                .client(okHttpClient)
                .build()
                .create(AuthHttpApi.class);
    }

    @Override
    public ApiResponse register(User user) throws Exception {
        if (!Utils.ConnectionChecker.hasConnection(context)) {
            return new ApiResponse(Utils.Constants.APIRESPONSECODES.ERROR, "No Internet " +
                    "Connection");
        }
        final String un = user.getUn();
        final String pw = user.getPw();
        final String email = user.getEmail();
        final RequestBody requestBody = createRegisterRequestBody(un, pw, email);
        final Call<ApiResponse> call = authParserImpl.register(requestBody);
        try {
            final Response<ApiResponse> response = call.execute();
            Preconditions.checkState(response.code() == Utils.Constants.HTTPCODES.SC_CREATED);
            final ApiResponse apiResponse = response.body();
            return apiResponse;
        } catch (Exception e) {
            throw new Exception(e);
        }
    }

    @Override
    public ApiResponse login(@NonNull User user) throws Exception {
        if (!Utils.ConnectionChecker.hasConnection(context)) {
            return new ApiResponse(Utils.Constants.APIRESPONSECODES.ERROR, "No Internet " +
                    "Connection");
        }
        final String un = user.getUn();
        final String pw = user.getPw();
        final RequestBody requestBody = createLoginRequestBody(un, pw);
        final Call<ApiResponse> call = authParserImpl.login(requestBody);
        try {
            final Response<ApiResponse> response = call.execute();
            Preconditions.checkState(response.code() == Utils.Constants.HTTPCODES.SC_CREATED);
            final ApiResponse apiResponse = response.body();
            return apiResponse;
        } catch (Exception e) {
            throw new Exception(e);
        }
    }

    private RequestBody createLoginRequestBody(String un, String pw) {
        final JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(Utils.Constants.USERCONST.PARAMETER_USERNAME, un);
            jsonObject.put(Utils.Constants.USERCONST.PARAMETER_PASSWORD, pw);
            final MediaType mediaType = MediaType.parse(MEDIA_TYPE_APPLICATION_JSON);
            final String content = jsonObject.toString();
            return RequestBody.create(mediaType, content);
        } catch (JSONException e) {
            e.printStackTrace();
            throw new IllegalStateException("Could not create Login Http Post request!");
        }
    }

    private RequestBody createRegisterRequestBody(String un, String pw, String email) {
        final JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(Utils.Constants.USERCONST.PARAMETER_USERNAME, un);
            jsonObject.put(Utils.Constants.USERCONST.PARAMETER_PASSWORD, pw);
            jsonObject.put(Utils.Constants.USERCONST.PARAMETER_EMAIL, email);
            final MediaType mediaType = MediaType.parse(MEDIA_TYPE_APPLICATION_JSON);
            final String content = jsonObject.toString();
            return RequestBody.create(mediaType, content);
        } catch (JSONException e) {
            e.printStackTrace();
            throw new IllegalStateException("Could not create Register Http Post request!");
        }
    }

    @Override
    public void injectMembers() {
        DependencyInjector.getGraph().inject(this);
    }
}
