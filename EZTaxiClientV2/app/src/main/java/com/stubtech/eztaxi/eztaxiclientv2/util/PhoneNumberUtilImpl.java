package com.stubtech.eztaxi.eztaxiclientv2.util;

import com.google.i18n.phonenumbers.AsYouTypeFormatter;
import com.google.i18n.phonenumbers.NumberParseException;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.Phonenumber;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Locale;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class PhoneNumberUtilImpl {

    private final PhoneNumberUtil phoneUtil;
    private final ArrayList<PhoneNumberInfo> phoneNumbersInfo;
    private final String[] countryNamesAndPhoneNumbers;
    private final HashMap<Integer, Integer> countryCodesMap;
    private final ArrayList<String> countryCodes;

    @Inject
    public PhoneNumberUtilImpl() {
        phoneUtil = PhoneNumberUtil.getInstance();
        phoneNumbersInfo = new ArrayList<>();
        countryCodesMap = new HashMap<>();
        countryCodes = new ArrayList<>(phoneUtil.getSupportedRegions());
        int size = countryCodes.size();
        countryNamesAndPhoneNumbers = new String[size];
        Locale l;
        for (int i = 0; i < size; i++) {
            String countryCode = countryCodes.get(i);
            l = new Locale("", countryCode);
            String countryName = l.getDisplayCountry();
            int code = phoneUtil.getCountryCodeForRegion(countryCode);
            phoneNumbersInfo.add(new PhoneNumberInfo(countryName, countryCode, code));
        }
        Collections.sort(phoneNumbersInfo);
        for (int i = 0; i < phoneNumbersInfo.size(); i++) {
            String customValue = phoneNumbersInfo.get(i).countryName + " +(" + phoneNumbersInfo
                    .get(i).code + ")";
            countryNamesAndPhoneNumbers[i] = customValue;
            countryCodesMap.put(i, phoneNumbersInfo.get(i).code);
        }
    }

    public String getCountryCode(int pos) {
        return phoneNumbersInfo.get(pos).countryCode;
    }

    public boolean validateNumber(String number, int position) {
        Phonenumber.PhoneNumber phoneNumber;
        try {
            phoneNumber = phoneUtil.parse(number, phoneNumbersInfo.get(position).countryCode);
        } catch (NumberParseException e) {
            System.err.println("NumberParseException was thrown: " + e.toString());
            return false;
        }
        return phoneUtil.isValidNumber(phoneNumber);
    }

    public ArrayList<PhoneNumberInfo> getPhoneNumbersInfo() {
        return phoneNumbersInfo;
    }

    public String[] getCountryNamesAndPhoneNumbers() {
        return countryNamesAndPhoneNumbers;
    }

    public HashMap<Integer, Integer> getCountryCodesMap() {
        return countryCodesMap;
    }

    public String getFormattedNumber(String number, int selectedCountryCodePosition) {
        AsYouTypeFormatter formatter = phoneUtil.getAsYouTypeFormatter(getCountryCode
                (selectedCountryCodePosition));
        String result = "";
        for (char c : number.toCharArray()) {
            if (c >= '0' && c <= '9') result = formatter.inputDigit(c);
        }
        return result;
    }

    public final class PhoneNumberInfo implements Comparable {

        public final String countryName;
        public final String countryCode;
        public final int code;

        public PhoneNumberInfo(String countryName, String countryCode, int code) {
            this.countryName = countryName;
            this.countryCode = countryCode;
            this.code = code;
        }

        @Override
        public int compareTo(Object another) {
            return this.countryName.compareTo(((PhoneNumberInfo) another).countryName);
        }
    }
}
