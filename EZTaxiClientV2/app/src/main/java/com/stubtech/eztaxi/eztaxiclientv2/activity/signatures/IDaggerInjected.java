package com.stubtech.eztaxi.eztaxiclientv2.activity.signatures;

public interface IDaggerInjected {

    void injectMembers();
}
