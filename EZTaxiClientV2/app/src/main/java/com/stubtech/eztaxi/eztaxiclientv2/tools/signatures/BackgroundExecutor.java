package com.stubtech.eztaxi.eztaxiclientv2.tools.signatures;

import com.stubtech.eztaxi.eztaxiclientv2.di.signatures.Destroyable;

import rx.functions.Action0;

/**
 * Interface to perform actions on separate thread from main ui thread.
 * Substitutes AsyncTask
 */
public interface BackgroundExecutor extends Destroyable {

    void execute(Action0 action);
    void destroy();
}
