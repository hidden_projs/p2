package com.stubtech.eztaxi.eztaxiclientv2.activity.controller.signatures;

import com.google.common.eventbus.Subscribe;
import com.stubtech.eztaxi.eztaxiclientv2.di.signatures.Destroyable;
import com.stubtech.eztaxi.eztaxiclientv2.pojo.ApiResponse;

public interface ILoginActivityController extends Destroyable {

    void attemptLogin();
    @Subscribe
    void onEvent(ApiResponse event);
    void confirmAuthentication(String token);
    void showAuthenticationError(String error);
    void goToRegister();
    boolean validateUserInput();
}
