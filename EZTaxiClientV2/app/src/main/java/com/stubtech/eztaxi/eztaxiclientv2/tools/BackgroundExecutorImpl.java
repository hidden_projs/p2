package com.stubtech.eztaxi.eztaxiclientv2.tools;

import com.google.common.collect.Lists;
import com.stubtech.eztaxi.eztaxiclientv2.di.signatures.Destroyable;
import com.stubtech.eztaxi.eztaxiclientv2.tools.signatures.BackgroundExecutor;

import java.util.Collection;

import rx.Subscription;
import rx.functions.Action0;
import rx.schedulers.Schedulers;

/**
 * Implementation class for communication in background
 *
 * @see com.stubtech.eztaxi.eztaxiclientv2.tools.BackgroundExecutorImpl
 */
public class BackgroundExecutorImpl implements BackgroundExecutor, Destroyable {

    private final Collection<Subscription> subscriptions = Lists.newLinkedList();

    @Override
    public synchronized void execute(Action0 action) {
        final Subscription subscription = Schedulers.newThread().createWorker().schedule(action);
        subscriptions.add(subscription);
    }

    @Override
    public synchronized void destroy() {
        for (Subscription subscription : subscriptions) {
            subscription.unsubscribe();
        }
        subscriptions.clear();
    }
}
