package com.stubtech.eztaxi.eztaxiclientv2.service;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;

/**
 * Used to refresh token for authentication
 */
public class TokenAuthService extends Service {

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}
