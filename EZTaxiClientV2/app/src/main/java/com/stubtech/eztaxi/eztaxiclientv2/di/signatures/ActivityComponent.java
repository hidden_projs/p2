package com.stubtech.eztaxi.eztaxiclientv2.di.signatures;

import com.stubtech.eztaxi.eztaxiclientv2.activity.HomeActivity;
import com.stubtech.eztaxi.eztaxiclientv2.activity.LoginActivity;
import com.stubtech.eztaxi.eztaxiclientv2.activity.RegisterActivity;
import com.stubtech.eztaxi.eztaxiclientv2.di.module.ActivityModule;

import dagger.Component;

/**
 * Defines Dagger graph for the activity scope
 */
@ActivityScope
@Component(dependencies = DependencyGraph.class,
        modules = {ActivityModule.class})
public interface ActivityComponent {

    void inject(HomeActivity homeActivity);
    void inject(RegisterActivity registerActivity);
    void inject(LoginActivity loginActivity);
}
