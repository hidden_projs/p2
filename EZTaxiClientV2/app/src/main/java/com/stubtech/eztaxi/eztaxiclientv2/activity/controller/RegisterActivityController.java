package com.stubtech.eztaxi.eztaxiclientv2.activity.controller;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;

import com.google.common.base.Preconditions;
import com.google.common.eventbus.Subscribe;
import com.stubtech.eztaxi.eztaxiclientv2.activity.LoginActivity;
import com.stubtech.eztaxi.eztaxiclientv2.activity.RegisterActivity;
import com.stubtech.eztaxi.eztaxiclientv2.activity.controller.signatures
        .IRegisterActivityController;
import com.stubtech.eztaxi.eztaxiclientv2.activity.signatures.IRegisterView;
import com.stubtech.eztaxi.eztaxiclientv2.di.signatures.ActivityScope;
import com.stubtech.eztaxi.eztaxiclientv2.pojo.ApiResponse;
import com.stubtech.eztaxi.eztaxiclientv2.pojo.User;
import com.stubtech.eztaxi.eztaxiclientv2.security.AppSecurityManager;
import com.stubtech.eztaxi.eztaxiclientv2.security.RegisterTask;
import com.stubtech.eztaxi.eztaxiclientv2.security.signatures.AuthenticationProvider;
import com.stubtech.eztaxi.eztaxiclientv2.tools.signatures.BroadcastManager;
import com.stubtech.eztaxi.eztaxiclientv2.util.PhoneNumberUtilImpl;
import com.stubtech.eztaxi.eztaxiclientv2.util.Utils;

import javax.inject.Inject;

import static android.R.layout.simple_spinner_dropdown_item;

@ActivityScope
public class RegisterActivityController implements IRegisterActivityController {

    private final IRegisterView view;
    private final BroadcastManager broadcastManager;
    private final Context context;
    private final AuthenticationProvider authenticationProvider;
    private final PhoneNumberUtilImpl phoneNumberUtil;
    private int selectedCountryCodePosition = 0;

    @Inject
    public RegisterActivityController(@NonNull IRegisterView view,
                                      @NonNull BroadcastManager broadcastManager,
                                      @NonNull Context context,
                                      @NonNull AuthenticationProvider authenticationProvider,
                                      @NonNull PhoneNumberUtilImpl phoneNumberUtil) {
        Preconditions.checkNotNull(broadcastManager);
        Preconditions.checkNotNull(view);
        Preconditions.checkNotNull(context);
        Preconditions.checkNotNull(authenticationProvider);
        Preconditions.checkNotNull(phoneNumberUtil);
        this.broadcastManager = broadcastManager;
        this.view = view;
        this.context = context;
        this.authenticationProvider = authenticationProvider;
        this.phoneNumberUtil = phoneNumberUtil;
    }

    @Override
    public void attemptRegister() {
        if (validateUserInput()) {
            User user = new User();
            user.setUn(view.getmTxtUN().getText().toString().trim());
            user.setPw(view.getmTxtPWFirst().getText().toString().trim());
            user.setPhoneNumber(view.getmTxtNumber().getText().toString().trim());
            view.getmBtnRegister().setEnabled(false);
            view.getmBtnRegister().setAlpha(.5f);
            broadcastManager.register(this);
            new RegisterTask(authenticationProvider, broadcastManager).execute(user);
        }
    }

    @Override
    @Subscribe
    public void onEvent(ApiResponse event) {
        broadcastManager.unregister(this);
        view.getmBtnRegister().setEnabled(true);
        view.getmBtnRegister().setAlpha(1f);
        if (event.getCode() == Utils.Constants.APIRESPONSECODES.OK) {
            confirmRegistration(event.getPayload());
        } else {
            showRegistrationError(event.getPayload());
        }
    }

    @Override
    public void confirmRegistration(String token) {
        AppSecurityManager.logIn(token);
        Intent intent = new Intent(context, RegisterActivity.class);
        ((AppCompatActivity) view).startActivity(intent);
        ((AppCompatActivity) view).finishActivity(0);
    }

    @Override
    public void showRegistrationError(String error) {
        Utils.ToastCreator.showToast((AppCompatActivity) view, error);
    }

    @Override
    public void goToLogin() {
        Intent intent = new Intent(context, LoginActivity.class);
        ((AppCompatActivity) view).startActivity(intent);
        ((AppCompatActivity) view).finishActivity(0);
    }

    @Override
    public boolean validateUserInput() {
        boolean valid = true;
        String email = view.getmTxtUN().getText().toString().trim();
        String pw_first = view.getmTxtPWFirst().getText().toString().trim();
        String pw_second = view.getmTxtPWSecond().getText().toString().trim();
        String number = view.getmTxtNumber().getText().toString().trim();
        if (email.isEmpty() || !android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            view.getmWrapperUN().setError("enter a valid email address");
            valid = false;
        } else {
            view.getmWrapperUN().setError(null);
        }
        if (pw_first.isEmpty() || pw_first.length() < 4 || pw_first.length() > 10) {
            view.getmWrapperPWFirst().setError("between 4 and 10 alphanumeric characters");
            valid = false;
        } else {
            view.getmWrapperPWFirst().setError(null);
        }
        if (pw_second.isEmpty() || !pw_second.equals(pw_first)) {
            view.getmWrapperPWSecond().setError("Passwords do not match");
            valid = false;
        } else {
            view.getmWrapperPWSecond().setError(null);
        }
        if (number.isEmpty() || validatePhoneNumber(number)) {
            view.getmWrapperNumber().setError("Invalid phone number");
            valid = false;
        } else {
            view.getmWrapperNumber().setError(null);
        }
        return valid;
    }

    public boolean validatePhoneNumber(String number) {
        return phoneNumberUtil.validateNumber(number, selectedCountryCodePosition);
    }

    @Override
    public void destroy() {
        broadcastManager.unregister(this);
    }

    public void instantiatePhoneNumberUtils() {
        ArrayAdapter<String> stringArrayAdapter = new ArrayAdapter<>((AppCompatActivity) view,
                android.R.layout.simple_spinner_item, phoneNumberUtil
                .getCountryNamesAndPhoneNumbers());
        stringArrayAdapter.setDropDownViewResource(simple_spinner_dropdown_item);
        view.getmSpinnerCountryCode().setAdapter(stringArrayAdapter);
        //TODO: Get geo location and assyst with country selection;
        view.getmSpinnerCountryCode().setOnItemSelectedListener(new OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
                try {
                    selectedCountryCodePosition = arg2;
                    view.getmTxtCountryCode()
                            .setText("(" + phoneNumberUtil.getCountryCodesMap().get(arg2) + ")");
                } catch (Exception e) {
                    view.getmTxtCountryCode().setText("()");
                    String s = e.getMessage();
                    System.out.println(s);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                try {
                    android.os.Debug.waitForDebugger();
                    view.getmTxtCountryCode().setText("()");
                } catch (Exception e) {
                    android.os.Debug.waitForDebugger();
                    String s = e.getMessage();
                    System.out.println(s);
                }
            }
        });
    }
}
