package com.stubtech.eztaxi.eztaxiclientv2;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.stubtech.eztaxi.eztaxiclientv2.di.DependencyInjector;
import com.stubtech.eztaxi.eztaxiclientv2.di.module.ApplicationModule;
import com.stubtech.eztaxi.eztaxiclientv2.di.module.AuthenticationModule;
import com.stubtech.eztaxi.eztaxiclientv2.di.module.BroadcastModule;
import com.stubtech.eztaxi.eztaxiclientv2.di.module.HttpModule;
import com.stubtech.eztaxi.eztaxiclientv2.di.module.MemoryLeakDetectionModule;
import com.stubtech.eztaxi.eztaxiclientv2.di.signatures.ModuleSupplier;
import com.stubtech.eztaxi.eztaxiclientv2.util.Utils;

/**
 * Application class itself.
 * <p/>
 * Instantiation is necessary for "bootstrap injection", so I can use Dependency Graph for Dagger
 */
public class EZTaxiClientApplication extends Application implements ModuleSupplier {

    public static SharedPreferences preferences;

    @Override
    public void onCreate() {
        Log.d(Utils.Constants.DEBUG_TAG, "Started app Initialisation");
        // @First method to be called in Application lifecycle
        super.onCreate();
        /* Instantiate application wide SharedPreferences for storing simple primitives (like
        booleans) or serializable objects */
        preferences = getSharedPreferences(getPackageName() + "_preferences", MODE_PRIVATE);
        Utils.FacebookDebugBridge.installStetho(this);
        Log.d(Utils.Constants.DEBUG_TAG, "Initialised Application");
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        DependencyInjector.initialize(this);
        Log.d(Utils.Constants.DEBUG_TAG, "Injected Application");
    }

    @Override
    public ApplicationModule provideApplicationModule() {
        return new ApplicationModule(this);
    }

    @Override
    public AuthenticationModule provideAuthenticationModule() {
        return new AuthenticationModule();
    }

    @Override
    public BroadcastModule provideBroadcastModule() {
        return new BroadcastModule();
    }

    @Override
    public HttpModule provideHttpModule() {
        return new HttpModule();
    }

    @Override
    public MemoryLeakDetectionModule provideMemoryLeakDetectionModule() {
        return new MemoryLeakDetectionModule(BuildConfig.DEBUG, this);
    }
}

