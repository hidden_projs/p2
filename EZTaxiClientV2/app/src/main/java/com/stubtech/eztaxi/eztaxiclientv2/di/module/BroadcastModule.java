package com.stubtech.eztaxi.eztaxiclientv2.di.module;

import com.stubtech.eztaxi.eztaxiclientv2.BuildConfig;
import com.stubtech.eztaxi.eztaxiclientv2.tools.BroadcastManagerImpl;
import com.stubtech.eztaxi.eztaxiclientv2.tools.signatures.BroadcastManager;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import de.greenrobot.event.EventBus;

/**
 * This module provides:
 * <p/>
 * Enhancement over Intents
 * Android optimized event bus that simplifies communication between Activities, Fragments,
 * Threads, Services, etc. Less code, better quality. http://greenrobot.org/eventbus/
 * <p/>
 * API describes it enough :)
 */
@Module
public class BroadcastModule {

    @Provides
    @Singleton
    public BroadcastManager provideBroadcastManager() {
        final boolean isDebugBuild = BuildConfig.DEBUG;
        final EventBus eventBus = EventBus.builder()
                .logNoSubscriberMessages(isDebugBuild)
                .logSubscriberExceptions(isDebugBuild)
                .build();
        return new BroadcastManagerImpl(eventBus);
    }
}
