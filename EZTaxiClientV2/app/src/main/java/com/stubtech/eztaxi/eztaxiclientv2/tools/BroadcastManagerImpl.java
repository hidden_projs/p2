package com.stubtech.eztaxi.eztaxiclientv2.tools;

import android.support.annotation.NonNull;

import com.google.common.base.Preconditions;
import com.stubtech.eztaxi.eztaxiclientv2.tools.signatures.BroadcastManager;

import de.greenrobot.event.EventBus;

/**
 * Implementation class for broadcasting objects
 *
 * @see com.stubtech.eztaxi.eztaxiclientv2.tools.signatures.BroadcastManager
 */
public class BroadcastManagerImpl implements BroadcastManager {

    private final EventBus eventBus;

    public BroadcastManagerImpl(EventBus eventBus) {
        Preconditions.checkNotNull(eventBus);
        this.eventBus = eventBus;
    }

    @Override
    public void register(@NonNull Object object) {
        Preconditions.checkNotNull(object);
        try {
            if (!eventBus.isRegistered(object)) {
                eventBus.register(object);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void unregister(@NonNull Object object) {
        Preconditions.checkNotNull(object);
        eventBus.unregister(object);
    }

    @Override
    public void post(@NonNull Object object) {
        Preconditions.checkNotNull(object);
        eventBus.post(object);
    }
}
