package com.stubtech.eztaxi.eztaxiclientv2.activity.homefragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.SwitchCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.Spinner;

import com.stubtech.eztaxi.eztaxiclientv2.R;
import com.stubtech.eztaxi.eztaxiclientv2.activity.homefragments.signatures.IChooseOptionsFragment;
import com.stubtech.eztaxi.eztaxiclientv2.activity.signatures.IButteredKnifeActivity;
import com.stubtech.eztaxi.eztaxiclientv2.util.Utils;

import butterknife.Bind;
import butterknife.ButterKnife;

import static android.R.layout.simple_spinner_dropdown_item;

public class ChooseOptionsFragment extends Fragment implements IChooseOptionsFragment,
                                                               IButteredKnifeActivity {

    @Bind(R.id.home_choose_opt_fragment_car_type_spinner)
    Spinner mCarTypeSpinner;

    @Bind(R.id.home_choose_opt_fragment_payment_type_spinner)
    Spinner mPaymentTypeSpinner;

    @Bind(R.id.home_choose_opt_fragment_child_seat_switch)
    SwitchCompat mChildSeatSwitch;

    @Bind(R.id.home_choose_opt_fragment_pet_switch)
    SwitchCompat mPetTransportedSwitch;

    View view;

    public ChooseOptionsFragment() {
        System.out.println();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.activity_home_fragment_choose_options, container, false);
        bindViews();
        setUp();
        return view;
    }

    private void setUp() {
        //TODO: REIMPLEMENT USING DAGGER
        String carTypes[] = {"6-seater", "4-seater"};
        String paymentTypes[] = {"Cash to driver", "Card"};
        ArrayAdapter<String> carStringArrayAdapter = new ArrayAdapter<>(getContext(), android.R
                .layout.simple_spinner_item, carTypes);
        ArrayAdapter<String> paymentStringArrayAdapter = new ArrayAdapter<>(getContext(), android
                .R.layout.simple_spinner_item, paymentTypes);
        carStringArrayAdapter.setDropDownViewResource(simple_spinner_dropdown_item);
        paymentStringArrayAdapter.setDropDownViewResource(simple_spinner_dropdown_item);
        mPaymentTypeSpinner.setAdapter(paymentStringArrayAdapter);
        mCarTypeSpinner.setAdapter(carStringArrayAdapter);
        mChildSeatSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                Utils.ToastCreator.showToast(getContext(), "HELLO");
            }
        });
    }

    @Override
    public Spinner getmCarTypeSpinner() {
        return mCarTypeSpinner;
    }

    @Override
    public Spinner getmPaymentTypeSpinner() {
        return mPaymentTypeSpinner;
    }

    @Override
    public SwitchCompat getmChildSeatSwitch() {
        return mChildSeatSwitch;
    }

    @Override
    public SwitchCompat getmPetTransportedSwitch() {
        return mPetTransportedSwitch;
    }

    @Override
    public void bindViews() {
        ButterKnife.bind(this, view);
    }
}
