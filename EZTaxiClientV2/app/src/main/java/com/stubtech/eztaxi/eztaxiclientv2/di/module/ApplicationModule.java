package com.stubtech.eztaxi.eztaxiclientv2.di.module;

import android.accounts.AccountManager;
import android.content.Context;
import android.content.res.Resources;

import com.stubtech.eztaxi.eztaxiclientv2.EZTaxiClientApplication;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * This is the main module of the application.
 * <p/>
 * Injects Android Account Manager.
 * Injects Application to Dagger.
 */
@Module
public class ApplicationModule {

    private final EZTaxiClientApplication application;

    public ApplicationModule(EZTaxiClientApplication application) {
        this.application = application;
    }

    @Provides
    @Singleton
    public AccountManager provideAccountManager() {
        return AccountManager.get(application);
    }

    @Provides
    @Singleton
    public EZTaxiClientApplication provideApplication() {
        return application;
    }

    @Provides
    @Singleton
    public Context provideApplicationContext() {
        return application;
    }

    @Provides
    @Singleton
    public Resources provideResources() {
        return application.getResources();
    }
}
