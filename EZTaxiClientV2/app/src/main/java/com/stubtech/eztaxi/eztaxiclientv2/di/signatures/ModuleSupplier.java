package com.stubtech.eztaxi.eztaxiclientv2.di.signatures;

import com.stubtech.eztaxi.eztaxiclientv2.di.module.ApplicationModule;
import com.stubtech.eztaxi.eztaxiclientv2.di.module.AuthenticationModule;
import com.stubtech.eztaxi.eztaxiclientv2.di.module.BroadcastModule;
import com.stubtech.eztaxi.eztaxiclientv2.di.module.HttpModule;
import com.stubtech.eztaxi.eztaxiclientv2.di.module.MemoryLeakDetectionModule;

/**
 * Interface that lists all modules to use as singletons for app. Subclasses can override this
 * method to
 * provide additional modules provided they call {@code super.getModules()}.
 */
public interface ModuleSupplier {

    ApplicationModule provideApplicationModule();
    AuthenticationModule provideAuthenticationModule();
    BroadcastModule provideBroadcastModule();
    HttpModule provideHttpModule();
    MemoryLeakDetectionModule provideMemoryLeakDetectionModule();
}
