package com.stubtech.eztaxi.eztaxiclientv2.di.signatures;

import android.content.Context;

import com.squareup.leakcanary.RefWatcher;
import com.stubtech.eztaxi.eztaxiclientv2.di.module.ApplicationModule;
import com.stubtech.eztaxi.eztaxiclientv2.di.module.AuthenticationModule;
import com.stubtech.eztaxi.eztaxiclientv2.di.module.BackgroundExecutorModule;
import com.stubtech.eztaxi.eztaxiclientv2.di.module.BroadcastModule;
import com.stubtech.eztaxi.eztaxiclientv2.di.module.HttpModule;
import com.stubtech.eztaxi.eztaxiclientv2.di.module.MemoryLeakDetectionModule;
import com.stubtech.eztaxi.eztaxiclientv2.security.signatures.AuthenticationProvider;
import com.stubtech.eztaxi.eztaxiclientv2.tools.signatures.BackgroundExecutor;
import com.stubtech.eztaxi.eztaxiclientv2.tools.signatures.BroadcastManager;
import com.stubtech.eztaxi.eztaxiclientv2.util.PhoneNumberUtilImpl;

import javax.inject.Singleton;

import dagger.Component;
import dagger.Lazy;
/**
 * The injector class for Dagger.
 * <p/>
 * Assigns references in activities, services, or fragments to have access to singletons
 * <p/>
 * Singleton -> single value created for all usages aka Application Scoped
 * Component -> marks class for Dagger to be included for dependency injection
 */

/**
 * Explicitly Inject Modules, modules are like Config files in spring,
 * Inside which I can create Beans
 */
@Singleton
@Component(modules = {
        // Main Application module
        ApplicationModule.class,
        // Authentication Module
        AuthenticationModule.class,
        // Module used to exchange messages
        BroadcastModule.class,
        // Module used to communicate over HTTP
        HttpModule.class,
        // Module used to execute tasks in background, not on the main ui thread
        BackgroundExecutorModule.class,
        // Module used for debug purposes
        MemoryLeakDetectionModule.class})
public interface DependencyGraph {

    /*
     * To inject fields in a class it has to call inject() method.
     * Each such class has to have explicit definition below
     */

    void inject(AuthenticationProvider authenticationProvider);

    /*
     * When creating dependent components, the parent component needs to explicitly expose the
     * objects to downstream objects. For example, if a downstream component needed access to the
     * Retrofit instance, it would need to explicitly expose it with the corresponding return type
     *
     * Eg my dependant downstream component is Activity an ActivityModule graph.
     */
    AuthenticationProvider authenticationProvider();
    Context context();
    BroadcastManager broadcastManager();
    BackgroundExecutor backgroundExecutor();
    // Watches after leaked memory, and kills what should not be alive
    Lazy<RefWatcher> refWatcher();
    // provides tools to manage phone numbers
    PhoneNumberUtilImpl phoneNumberUtil();
}