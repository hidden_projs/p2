package main.java.com.stubtech.eztaxi.commonmodels.models;

/**
 * Created by Batousik on 26.02.2016.
 */
public class TaxiOrderForm {
    private String addressFrom;
    private String addressTo;
    private Long pickUpTimeMillis;
    private String carType;
    private String paymentType;
    private String comment;
    private String[] requirements;

    public String getAddressFrom() {
        return addressFrom;
    }

    public void setAddressFrom(String addressFrom) {
        this.addressFrom = addressFrom;
    }

    public String getAddressTo() {
        return addressTo;
    }

    public void setAddressTo(String addressTo) {
        this.addressTo = addressTo;
    }

    public Long getPickUpTimeMillis() {
        return pickUpTimeMillis;
    }

    public void setPickUpTimeMillis(Long pickUpTimeMillis) {
        this.pickUpTimeMillis = pickUpTimeMillis;
    }

    public String getCarType() {
        return carType;
    }

    public void setCarType(String carType) {
        this.carType = carType;
    }

    public String getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String[] getRequirements() {
        return requirements;
    }

    public void setRequirements(String[] requirements) {
        this.requirements = requirements;
    }

    @Override
    public String toString() {
        return addressFrom + "\n" + addressTo + "\n"
                + pickUpTimeMillis + "\n"+ carType + "\n"
                + paymentType + "\n"+ comment + "\n";
    }
}
