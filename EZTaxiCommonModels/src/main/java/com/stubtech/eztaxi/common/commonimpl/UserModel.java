package com.stubtech.eztaxi.common.commonimpl;

import com.stubtech.eztaxi.common.commoninterface.IUserModel;


public class UserModel implements IUserModel {
    private String username;
    private String password;
    private String email;
    private String role;

    public UserModel() {
    }

    @Override
    public String getUsername() {
        return username;
    }

    @Override
    public void setUsername(String username) {
        this.username = username;
    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String getRole() {
        return role;
    }

    @Override
    public void setRole(String role) {
        this.role = role;
    }
}
