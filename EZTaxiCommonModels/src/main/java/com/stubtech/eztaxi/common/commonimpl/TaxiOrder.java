package com.stubtech.eztaxi.common.commonimpl;

import com.stubtech.eztaxi.common.commoninterface.ITaxiOrder;

/**
 * Created by Batousik on 26.02.2016.
 */
public class TaxiOrder implements ITaxiOrder {
    private String addressFrom;
    private String addressTo;
    private Long pickUpTimeMillis;
    private String carType;
    private String paymentType;
    private String comment;
    private String[] requirements;

    @Override
    public String getAddressFrom() {
        return addressFrom;
    }

    @Override
    public void setAddressFrom(String addressFrom) {
        this.addressFrom = addressFrom;
    }

    @Override
    public String getAddressTo() {
        return addressTo;
    }

    @Override
    public void setAddressTo(String addressTo) {
        this.addressTo = addressTo;
    }

    @Override
    public Long getPickUpTimeMillis() {
        return pickUpTimeMillis;
    }

    @Override
    public void setPickUpTimeMillis(Long pickUpTimeMillis) {
        this.pickUpTimeMillis = pickUpTimeMillis;
    }

    @Override
    public String getCarType() {
        return carType;
    }

    @Override
    public void setCarType(String carType) {
        this.carType = carType;
    }

    @Override
    public String getPaymentType() {
        return paymentType;
    }

    @Override
    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

    @Override
    public String getComment() {
        return comment;
    }

    @Override
    public void setComment(String comment) {
        this.comment = comment;
    }

    @Override
    public String[] getRequirements() {
        return requirements;
    }

    @Override
    public void setRequirements(String[] requirements) {
        this.requirements = requirements;
    }

    @Override
    public String toString() {
        return addressFrom + "\n" + addressTo + "\n"
                + pickUpTimeMillis + "\n"+ carType + "\n"
                + paymentType + "\n"+ comment + "\n";
    }
}
