package com.stubtech.eztaxi.common.commoninterface;


public interface ITaxiOrder {
    String getAddressFrom();

    void setAddressFrom(String addressFrom);

    String getAddressTo();

    void setAddressTo(String addressTo);

    Long getPickUpTimeMillis();

    void setPickUpTimeMillis(Long pickUpTimeMillis);

    String getCarType();

    void setCarType(String carType);

    String getPaymentType();

    void setPaymentType(String paymentType);

    String getComment();

    void setComment(String comment);

    String[] getRequirements();

    void setRequirements(String[] requirements);
}
