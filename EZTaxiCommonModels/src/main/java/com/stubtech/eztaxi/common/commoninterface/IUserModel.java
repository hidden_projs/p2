package com.stubtech.eztaxi.common.commoninterface;

/**
 * Created by Batousik on 28.02.2016.
 */
public interface IUserModel {
    String getUsername();

    void setUsername(String username);

    String getPassword();

    void setPassword(String password);

    String getRole();

    void setRole(String role);
}
